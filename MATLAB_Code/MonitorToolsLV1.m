% This program generates plots that evaluate the performance of the models
% and the rejection procedure.

%% Create Rejection rate vs property plot

% 1. count models that pass each property test and store the number in a vector.
PassCount(1) = length(RinPass_LV1);
PassCount(2) = length(VrestPass_LV1);
%PassCount(3) = length(TauPass_LV1);
PassCount(3) = length(APrePass_LV1);
PassCount(4) = length(PPrePass_LV1);
PassCount(5) = length(BPrePass_LV1);
PassCount(6) = length(APostPass_LV1);
PassCount(7) = length(BPostPass_LV1);
PassCount(8) = length(PassivePassIndx_LV1);
PassCount(9) = length(WavePassIndx_LV1);
PassCount(10) = length(PassIndxLV_1);

% 2. plot the vector.
figure('Name','Pass rate vs property')
bar(PassCount);
xticklabels({'Rin','Vrest','AreaPre','PeakPre','BaselinePre','AreaPost','BaselinePost','Passive','Wave','Overall'})
ylabel('Number of models that pass')
xlabel('Properties');

%% Examine the shape of post-rejection parameter space

% Change the source of GRangeBio to Para_LV1 if the goals is to compare
% pre- and post-selection size
h = Para_LV1;
h(2,:) = []; % discard Eleak

for i = 1:10
    GRangePre(:,i)= [min(h(i,:)) max(h(i,:))];
end

GRangePre(:,1:4) = GRangePre(:,1:4).*10^4;
GRangePre(:,5:10) = GRangePre(:,5:10).*10^5;
GRangePre(:,8) = GRangePre(:,8)./10;

% 1. store the original max and min G in a vector
GRangeBio(:,1)= [0.62 9.7];  %e-4
GRangeBio(:,2)= [1.72 19]; %e-4
GRangeBio(:,3)= [7.9 61];   %e-4
GRangeBio(:,4)= [1.65 12.7]; %e-4
GRangeBio(:,5)= [9.1 50];   %e-5
GRangeBio(:,6)= [6.5 13];   %e-5
GRangeBio(:,7)= [7 15];     %e-5   
GRangeBio(:,8)= [8.8 20];   %e-4
GRangeBio(:,9)= [16 31];    %e-5
GRangeBio(:,10)= [7.6 35];  %e-5

% 2. store the post-rejection max and min G in a vector
g = ParaPassLV_1;
g(2,:) = []; % discard Eleak

for i = 1:10
    GRangePost(:,i)= [min(g(i,:)) max(g(i,:))];
end

GRangePost(:,1:4) = GRangePost(:,1:4).*10^4;
GRangePost(:,5:10) = GRangePost(:,5:10).*10^5;
GRangePost(:,8) = GRangePost(:,8)./10;

% 3. plot them
figure('Name','Space shape comparison')
plot(GRangePre(2,:),'v','MarkerEdgeColor','k','MarkerFaceColor','k');
hold on
plot(GRangePre(1,:),'^','MarkerEdgeColor','k','MarkerFaceColor','k');
plot(GRangeBio(2,:),'v','MarkerEdgeColor','b');
hold on
plot(GRangeBio(1,:),'^','MarkerEdgeColor','b');
plot(GRangePost(2,:),'v','MarkerEdgeColor','r');
plot(GRangePost(1,:),'^','MarkerEdgeColor','r');
xticklabels({'Leak*e-4','A*e-4','BK*e-4','Kd1*e-4','Kd2*e-5','CaS*e-5',...
    'CAN*e-5','SK*e-4','CaT*e-5','NaP*e-5'})
ylabel('G (S/cm2)')
legend('PreSelect max','PreSelect min','Bio max','Bio min','PostSelect max','PostSelect min')

% 4. (Optional) put in models to compare
%GBad = Para_LV1(1:11,av);
%GBad(2,:) = [];
%GGood = Para_LV1(1:11,ainv);
%GGood(2,:) = [];
%Scale
%GBad(1:4,:) = GBad(1:4,:).*10^4;
%GBad(5:10,:) = GBad(5:10,:).*10^5;
%GBad(8,:) = GBad(8,:)./10;
%GGood(1:4,:) = GGood(1:4,:).*10^4;
%GGood(5:10,:) = GGood(5:10,:).*10^5;
%GGood(8,:) = GGood(8,:)./10;
%plot(GGood,'o','MarkerEdgeColor','g','MarkerFaceColor','g');
%plot(GBad,'o','MarkerEdgeColor','k');

%% Select some typical valid models and compare with the invalid ones  
% (need some manual selection and can adjust based on the result)
SynPrePass = SynPre_LV1(:,PassIndxLV_1);
SynPostPass = SynPost_LV1(:,PassIndxLV_1);
% 1. compare the model(s) with the (or almost) largest valid post-TEA area
am = find(AreaPost_LV1(1,:)>=max(AreaPost_LV1(1,PassIndxLV_1))-200);
av = intersect(am,PassIndxLV_1); % This will give the valid models
ainv = setxor(am,av); % This will give the models not in intersect = invalid

% 2. Pick several of them and plot (can adjust manually)
figure('Name','Valid Models With Max APost')
plot(subplot(1,2,1),SPtime,SynPre_LV1(:,av(2)));
ylim([-50 10]);
plot(subplot(1,2,2),SPtime,SynPost_LV1(:,av(2)));
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

figure('Name','Invalid Models With Max APost')
plot(subplot(1,2,1),SPtime,SynPre_LV1(:,1466));
ylim([-60 20]);
plot(subplot(1,2,2),SPtime,SynPost_LV1(:,1466));
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')
% 3.(Optional) compare the model(s) with the (or almost) largest valid pre-TEA Vpeak
pm = find(PeakPre_LV1(1,:)>=max(PeakPre_LV1(1,PassIndxLV_1))-2);
pv = intersect(pm,PassIndxLV_1); % This will give the valid models
pinv = setxor(pm,pv); % This will give the models not in intersect = invalid

% 4.(Optional) Pick several of them and plot (can adjust manually)
figure('Name','Valid Models With Max PPre')
plot(subplot(1,2,1),SPtime,SynPre_LV1(:,pv(2)));
ylim([-50 0]);
plot(subplot(1,2,2),SPtime,SynPost_LV1(:,pv(2)));
ylim([-50 0]);
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

figure('Name','Invalid Models With Max PPre')
plot(subplot(1,2,1),SPtime,SynPre_LV1(:,pinv(6)));
ylim([-60 10]);
plot(subplot(1,2,2),SPtime,SynPost_LV1(:,pinv(6)));
ylim([-60 10]);
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

% 5. other models

% 6. Random selection from the passed models
for n = 1:624
h = figure
picker = n;% PassIndxLV_1(random('unid',length(PassIndxLV_1)));
plot(subplot(1,2,1),SPtime,SynPre_LV1(:,picker));
title("PreTEA Cell " + picker)
ylim([-50 0]);
plot(subplot(1,2,2),SPtime,SynPost_LV1(:,picker));
title("PostTEA Cell " + picker)
ylim([-50 0]);
saveas(h, string(n),'jpg')
close
end

% 7. Plot all the models (not recommend)
for i = 1:length(PassIndxLV_1)
    figure()
    plot(SPtime,SynPost_LV1(:,PassIndxLV_1(i)));
end
%% Examine the correlation between each pair of G
% 1. Search for possible correlations between parameters
ParaCorLV_1 = ParaPassLV_1';    % The corr function compares column with column
ParaCorLV_1(:,2)=[];            % Discard ELeak

[R1,~] = corr(ParaCorLV_1,'type','Spearman');

% 2. Visualize the COC result
figure('Name','Matrix Plot')
plotmatrix(ParaCorLV_1);

figure('Name','Correlation Coefficients')
imagesc(abs(R1));
colormap('default');
colorbar;
set(gca, 'XTick', 1:10); % center x-axis ticks on bins
set(gca, 'YTick', 1:10); % center y-axis ticks on bins
xticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'Kd2', 'CaL', 'CAN', 'SKKCa', 'CaT', 'NaP'});
yticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'Kd2', 'CaL', 'CAN', 'SKKCa', 'CaT', 'NaP'});



