% This program will calculate the "Degree of Difference" between the cells
% generated in NEURON, which is represented by the Euclidean distance.
clear;
close all;

Trials = 1000; % Equals to the total number of cells generated
for i=1:Trials 
  fname = ['C:\Users\Neuro-Dustin\Desktop\RS_testingS2\Parameter_',num2str(i-1),'.txt'];
    Para(:,i) = load(fname);
end
Para(12:14,:)=[];   % No need for Vrest and so on
Para(2,:)=[];       % No need to include E-leak
Para = Para';
EuD = pdist(Para);
figure()
histogram(EuD);


% To interpret the degree of difference of a cell pool, we need to compare the Eu Distance with a reference.
% The "reference pairs" is obtained by replacing the "changing set" in each pair into the "non-changing set"
% but with K% larger conductance values.
% The bar chart will show us: At K% level of difference, Q% of the pairs
% have a Eu Distance > than their reference pairs.

% delete(PercentDiff);
% delete(refD);
assigner = 1;
for j = 1.05:0.05:1.6
    counter = 0;
    for k = 1:Trials-1
        ref1(1,:)=Para(k,:);
        ref1(2,:)=ref1(1,:)*j;
        for m = 0:Trials-k-1
            refD(m+counter+1)= pdist(ref1);
        end
        counter = counter+Trials-k;
    end
    PercentDiff(assigner) = sum(EuD>refD)/length(EuD)*100;
    assigner = assigner+1;
end
figure()
bar([5:5:60],PercentDiff);
ylabel('Percentage of Cell Pairs');
xlabel('Percentage Difference in Each Paramter');

% for i = 1:Trials
%     NPara(i,:)= Para(i,:)/norm(Para(i,:));
% end
% DP = double.empty(1,0);
% for i = 1:Trials-1
%     for j = i+1:Trials
%         DP(end+1)=dot(NPara(i,:),NPara(j,:),2);
%     end
% end
% DoD = EuD+DP;
% histogram(DP);
% figure()
% histogram(DoD);
