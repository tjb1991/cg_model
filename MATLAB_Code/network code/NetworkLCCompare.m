%% This program simply reads in all the output traces and displays them.

NetPre = importdata('..\..\lv3-out\NetPre.txt');
NetPost = importdata('..\..\lv3-out\NetPost.txt');
NetPreSIZ = importdata('..\..\lv3-out\NetPreSIZ.txt');
NetPostSIZ = importdata('..\..\lv3-out\NetPostSIZ.txt');

CellPre = importdata('..\..\lv3-out\CellPre.txt');
CellPost = importdata('..\..\lv3-out\CellPost.txt');
CellPreSIZ = importdata('..\..\lv3-out\CellPreSIZ.txt');
CellPostSIZ = importdata('..\..\lv3-out\CellPostSIZ.txt');

pointer = 100;%Network you want to plot currently

NetworkNum = 100;%Total number of networks
SCtime = [0:0.5:2500-0.5]';
% Cut some of the artifact data off
NetPre(5101:end,:)= [];
NetPost(5101:end,:)= [];
NetPreSIZ(5101:end,:)= [];
NetPostSIZ(5101:end,:)= [];

CellPre(5101:end,:)= [];
CellPost(5101:end,:)= [];
CellPreSIZ(5101:end,:)= [];
CellPostSIZ(5101:end,:)= [];

% Discard the first 50 ms data points to avoid V_init effect
NetPre(1:(50/0.5),:)= [];
NetPost(1:(50/0.5),:)= [];
NetPreSIZ(1:(50/0.5),:)= [];
NetPostSIZ(1:(50/0.5),:)= [];

CellPre(1:(50/0.5),:)= [];
CellPost(1:(50/0.5),:)= [];
CellPreSIZ(1:(50/0.5),:)= [];
CellPostSIZ(1:(50/0.5),:)= [];

counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,NetPre(:,counter+1))
    plot(ax2,SCtime,NetPre(:,counter+2))
    plot(ax3,SCtime,NetPre(:,counter+3))
    plot(ax4,SCtime,NetPre(:,counter+4))
    plot(ax5,SCtime,NetPre(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end

counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,NetPost(:,counter+1))
    plot(ax2,SCtime,NetPost(:,counter+2))
    plot(ax3,SCtime,NetPost(:,counter+3))
    plot(ax4,SCtime,NetPost(:,counter+4))
    plot(ax5,SCtime,NetPost(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end

counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,NetPreSIZ(:,counter+1))
    plot(ax2,SCtime,NetPreSIZ(:,counter+2))
    plot(ax3,SCtime,NetPreSIZ(:,counter+3))
    plot(ax4,SCtime,NetPreSIZ(:,counter+4))
    plot(ax5,SCtime,NetPreSIZ(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end

counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,NetPostSIZ(:,counter+1))
    plot(ax2,SCtime,NetPostSIZ(:,counter+2))
    plot(ax3,SCtime,NetPostSIZ(:,counter+3))
    plot(ax4,SCtime,NetPostSIZ(:,counter+4))
    plot(ax5,SCtime,NetPostSIZ(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end




counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,CellPre(:,counter+1))
    plot(ax2,SCtime,CellPre(:,counter+2))
    plot(ax3,SCtime,CellPre(:,counter+3))
    plot(ax4,SCtime,CellPre(:,counter+4))
    plot(ax5,SCtime,CellPre(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end

counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,CellPost(:,counter+1))
    plot(ax2,SCtime,CellPost(:,counter+2))
    plot(ax3,SCtime,CellPost(:,counter+3))
    plot(ax4,SCtime,CellPost(:,counter+4))
    plot(ax5,SCtime,CellPost(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end

counter = pointer;
for i = 1:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,CellPreSIZ(:,counter+1))
    plot(ax2,SCtime,CellPreSIZ(:,counter+2))
    plot(ax3,SCtime,CellPreSIZ(:,counter+3))
    plot(ax4,SCtime,CellPreSIZ(:,counter+4))
    plot(ax5,SCtime,CellPreSIZ(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end

counter = pointer;
for i = 1:NetworkNum
    figure
    ax1 = subplot(2,3,6);
    ax2 = subplot(2,3,5);
    ax3 = subplot(2,3,1);
    ax4 = subplot(2,3,2);
    ax5 = subplot(2,3,3);
    plot(ax1,SCtime,CellPostSIZ(:,counter+1))
    plot(ax2,SCtime,CellPostSIZ(:,counter+2))
    plot(ax3,SCtime,CellPostSIZ(:,counter+3))
    plot(ax4,SCtime,CellPostSIZ(:,counter+4))
    plot(ax5,SCtime,CellPostSIZ(:,counter+5))
    counter = counter + 5;
    xlim(ax1,[0 2500]);
    xlim(ax2,[0 2500]);
    xlim(ax3,[0 2500]);
    xlim(ax4,[0 2500]);
    xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
end