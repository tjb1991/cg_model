%This program merges output files from multiple Lewis jobs into one. 

NetPre1 = importdata('.\net1\NetPre.txt');
NetPost1 = importdata('.\net1\NetPost.txt');
NetPreSIZ1 = importdata('.\net1\NetPreSIZ.txt');
NetPostSIZ1 = importdata('.\net1\NetPostSIZ.txt');

CellPre1 = importdata('.\net1\CellPre.txt');
CellPost1 = importdata('.\net1\CellPost.txt');
CellPreSIZ1 = importdata('.\net1\CellPreSIZ.txt');
CellPostSIZ1 = importdata('.\net1\CellPostSIZ.txt');

NetPara1 = importdata('.\net1\NetworkParameter.txt');
NetCellID1 = importdata('.\net1\NetworkCellID.txt');
NetCellID1 = NetCellID1';
NetFN1 = importdata('.\net1\NetworkFN.txt');
NetFN1 = NetFN1.data;

NetPre1(5101:end,:)= [];
NetPost1(5101:end,:)= [];
NetPreSIZ1(5101:end,:)= [];
NetPostSIZ1(5101:end,:)= [];

CellPre1(5101:end,:)= [];
CellPost1(5101:end,:)= [];
CellPreSIZ1(5101:end,:)= [];
CellPostSIZ1(5101:end,:)= [];

NetPre2 = importdata('.\net2\NetPre.txt');
NetPost2 = importdata('.\net2\NetPost.txt');
NetPreSIZ2 = importdata('.\net2\NetPreSIZ.txt');
NetPostSIZ2 = importdata('.\net2\NetPostSIZ.txt');

CellPre2 = importdata('.\net2\CellPre.txt');
CellPost2 = importdata('.\net2\CellPost.txt');
CellPreSIZ2 = importdata('.\net2\CellPreSIZ.txt');
CellPostSIZ2 = importdata('.\net2\CellPostSIZ.txt');

NetPara2 = importdata('.\net2\NetworkParameter.txt');
NetCellID2 = importdata('.\net2\NetworkCellID.txt');
NetCellID2 = NetCellID2';
NetFN2 = importdata('.\net2\NetworkFN.txt');
NetFN2 = NetFN2.data;

NetPre2(5101:end,:)= [];
NetPost2(5101:end,:)= [];
NetPreSIZ2(5101:end,:)= [];
NetPostSIZ2(5101:end,:)= [];

CellPre2(5101:end,:)= [];
CellPost2(5101:end,:)= [];
CellPreSIZ2(5101:end,:)= [];
CellPostSIZ2(5101:end,:)= [];

NetPre3 = importdata('.\net3\NetPre.txt');
NetPost3 = importdata('.\net3\NetPost.txt');
NetPreSIZ3 = importdata('.\net3\NetPreSIZ.txt');
NetPostSIZ3 = importdata('.\net3\NetPostSIZ.txt');

CellPre3 = importdata('.\net3\CellPre.txt');
CellPost3 = importdata('.\net3\CellPost.txt');
CellPreSIZ3 = importdata('.\net3\CellPreSIZ.txt');
CellPostSIZ3 = importdata('.\net3\CellPostSIZ.txt');

NetPara3 = importdata('.\net3\NetworkParameter.txt');
NetCellID3 = importdata('.\net3\NetworkCellID.txt');
NetCellID3 = NetCellID3';
NetFN3 = importdata('.\net3\NetworkFN.txt');
NetFN3 = NetFN3.data;

NetPre3(5101:end,:)= [];
NetPost3(5101:end,:)= [];
NetPreSIZ3(5101:end,:)= [];
NetPostSIZ3(5101:end,:)= [];

CellPre3(5101:end,:)= [];
CellPost3(5101:end,:)= [];
CellPreSIZ3(5101:end,:)= [];
CellPostSIZ3(5101:end,:)= [];

NetPre4 = importdata('.\net4\NetPre.txt');
NetPost4 = importdata('.\net4\NetPost.txt');
NetPreSIZ4 = importdata('.\net4\NetPreSIZ.txt');
NetPostSIZ4 = importdata('.\net4\NetPostSIZ.txt');

CellPre4 = importdata('.\net4\CellPre.txt');
CellPost4 = importdata('.\net4\CellPost.txt');
CellPreSIZ4 = importdata('.\net4\CellPreSIZ.txt');
CellPostSIZ4 = importdata('.\net4\CellPostSIZ.txt');

NetPara4 = importdata('.\net4\NetworkParameter.txt');
NetCellID4 = importdata('.\net4\NetworkCellID.txt');
NetCellID4 = NetCellID4';
NetFN4 = importdata('.\net4\NetworkFN.txt');
NetFN4 = NetFN4.data;

NetPre4(5101:end,:)= [];
NetPost4(5101:end,:)= [];
NetPreSIZ4(5101:end,:)= [];
NetPostSIZ4(5101:end,:)= [];

CellPre4(5101:end,:)= [];
CellPost4(5101:end,:)= [];
CellPreSIZ4(5101:end,:)= [];
CellPostSIZ4(5101:end,:)= [];

NetPre5 = importdata('.\net5\NetPre.txt');
NetPost5 = importdata('.\net5\NetPost.txt');
NetPreSIZ5 = importdata('.\net5\NetPreSIZ.txt');
NetPostSIZ5 = importdata('.\net5\NetPostSIZ.txt');

CellPre5 = importdata('.\net5\CellPre.txt');
CellPost5 = importdata('.\net5\CellPost.txt');
CellPreSIZ5 = importdata('.\net5\CellPreSIZ.txt');
CellPostSIZ5 = importdata('.\net5\CellPostSIZ.txt');

NetPara5 = importdata('.\net5\NetworkParameter.txt');
NetCellID5 = importdata('.\net5\NetworkCellID.txt');
NetCellID5 = NetCellID5';
NetFN5 = importdata('.\net5\NetworkFN.txt');
NetFN5 = NetFN5.data;

NetPre5(5101:end,:)= [];
NetPost5(5101:end,:)= [];
NetPreSIZ5(5101:end,:)= [];
NetPostSIZ5(5101:end,:)= [];

CellPre5(5101:end,:)= [];
CellPost5(5101:end,:)= [];
CellPreSIZ5(5101:end,:)= [];
CellPostSIZ5(5101:end,:)= [];

%% Merge!
NetPre= [NetPre1,NetPre2,NetPre3,NetPre4,NetPre5];
NetPost= [NetPost1,NetPost2,NetPost3,NetPost4,NetPost5];
NetPreSIZ= [NetPreSIZ1,NetPreSIZ2,NetPreSIZ3,NetPreSIZ4,NetPreSIZ5];
NetPostSIZ= [NetPostSIZ1,NetPostSIZ2,NetPostSIZ3,NetPostSIZ4,NetPostSIZ5];

CellPre= [CellPre1,CellPre2,CellPre3,CellPre4,CellPre5];
CellPost= [CellPost1,CellPost2,CellPost3,CellPost4,CellPost5];
CellPreSIZ= [CellPreSIZ1,CellPreSIZ2,CellPreSIZ3,CellPreSIZ4,CellPreSIZ5];
CellPostSIZ= [CellPostSIZ1,CellPostSIZ2,CellPostSIZ3,CellPostSIZ4,CellPostSIZ5];

NetPara=[NetPara1,NetPara2,NetPara3,NetPara4,NetPara5] ;
NetCellID =[NetCellID1,NetCellID2,NetCellID3,NetCellID4,NetCellID5];
NetFN=[NetFN1,NetFN2,NetFN3,NetFN4,NetFN5];

