% This program import and analyze the LC network outputs.
% Then it will save the ID of valid LCs, i.e. networks.

%% Import data
NetPre = importdata('..\..\lv3-out\NetPre.txt');
NetPost = importdata('..\..\lv3-out\NetPost.txt');
NetPreSIZ = importdata('..\..\lv3-out\NetPreSIZ.txt');
NetPostSIZ = importdata('..\..\lv3-out\NetPostSIZ.txt');

CellPre = importdata('..\..\lv3-out\CellPre.txt');
CellPost = importdata('..\..\lv3-out\CellPost.txt');
CellPreSIZ = importdata('..\..\lv3-out\CellPreSIZ.txt');
CellPostSIZ = importdata('..\..\lv3-out\CellPostSIZ.txt');

NetPara = importdata('..\..\lv3-out\NetworkParameter.txt');
NetCellID = importdata('..\..\lv3-out\NetworkCellID.txt');
NetCellID = NetCellID';
NetFN = importdata('..\..\lv3-out\NetworkFN.txt');
NetFN = NetFN.data;

NetworkNum = 500;
SCtime = [0:0.5:2500-0.5]';
% Cut some of the artifact data off
NetPre(5101:end,:)= [];
NetPost(5101:end,:)= [];
NetPreSIZ(5101:end,:)= [];
NetPostSIZ(5101:end,:)= [];

CellPre(5101:end,:)= [];
CellPost(5101:end,:)= [];
CellPreSIZ(5101:end,:)= [];
CellPostSIZ(5101:end,:)= [];

% Discard the first 50 ms data points to avoid V_init effect
NetPre(1:(50/0.5),:)= [];
NetPost(1:(50/0.5),:)= [];
NetPreSIZ(1:(50/0.5),:)= [];
NetPostSIZ(1:(50/0.5),:)= [];

CellPre(1:(50/0.5),:)= [];
CellPost(1:(50/0.5),:)= [];
CellPreSIZ(1:(50/0.5),:)= [];
CellPostSIZ(1:(50/0.5),:)= [];

%% Analyze the outputs
peak_threshold = -25;
spike_width = 20;  %40*0.5 = 20 ms
SPBD = 1700; %ms, Set in NEURON, Netstim starts at 300 ms
CyclePeriod = 2500; %ms

for j = 1:NetworkNum*5
    
    y = NetPreSIZ(:,j);
    
    RMP(j) = y(SCtime ==(0));
    Vpeak(j) = max(y) - RMP(j);  % in mV
    [~, SpikeNum(j)] = bwlabel(y>peak_threshold); % ignore the first output
    [LCBD(j), Latency(j)] = BurstDurMeasure(SCtime, y, peak_threshold, spike_width);
    
    % We don't reject OffPhase because cycle period is arbitrary
    %slow_wave_depolarization_height = max(slow_y) - RMP  % in mV
    %plot(SCtime,slow_y); %Plot out the slow-wave oscillation
end

AvgFreq = SpikeNum*1000./LCBD; 
LCOnPhase = Latency/CyclePeriod;
LCOffPhase = (Latency+LCBD)/CyclePeriod; 

%% Rejection
SpikePass = double.empty(1,0);
OnPhasePass = double.empty(1,0);
FreqPass = double.empty(1,0);

for j = 1:NetworkNum*5 
    if SpikeNum(j)>=4&&SpikeNum(j)<=20
        SpikePass(end+1) = j;  
    end
    
    if LCOnPhase(j)>=0.058&&LCOnPhase(j)<=0.204
        OnPhasePass(end+1) = j;
    end
    
    if AvgFreq(j)>=2.04&&AvgFreq(j)<=13.7
        FreqPass(end+1)=j;
    end
end

%% Calculate the properties at soma
for i = 1:NetworkNum*5
    Baseline_Pre_LV3(1,i)= mean(NetPre(SCtime<=100,i));
    AreaPre_LV3(1,i) = abs(trapz(SCtime,(NetPre(:,i)-Baseline_Pre_LV3(1,i))));
    PeakPre_LV3(1,i) = max(NetPre(:,i))-Baseline_Pre_LV3(1,i);
    % Tag the cells
    Baseline_Pre_LV3(2,i)= i;
    AreaPre_LV3(2,i)= i;
    PeakPre_LV3(2,i)= i;
end

% Reject based on Pre-TEA response (Area under the curve)
APrePass_LV3 = double.empty(1,0);
for i = 1:length(AreaPre_LV3(1,:))
    if AreaPre_LV3(1,i)>=2867&&AreaPre_LV3(1,i)<=18373
       APrePass_LV3(end+1)=AreaPre_LV3(2,i);
    end
end

% Reject based on Pre-TEA response (maximum dV above baseline)
PPrePass_LV3 = double.empty(1,0);
for i = 1:length(PeakPre_LV3(1,:))
    if PeakPre_LV3(1,i)>=7.95&&PeakPre_LV3(1,i)<=33.76
        PPrePass_LV3(end+1)=PeakPre_LV3(2,i);
    end
end

C31 = intersect(SpikePass,FreqPass,'stable');
BurstPassIndx_LV3 = intersect(C31,OnPhasePass,'stable');
WavePassIndx_LV3 = intersect(APrePass_LV3,PPrePass_LV3,'stable');
C32 = intersect(WavePassIndx_LV3,BurstPassIndx_LV3,'stable');

% Run the "SynchronyTest.m" before proceeding

NetPassIndx_LV3 = intersect(C32,SyncPassCellID,'stable');

%% Extra parameters of interest that pass the rejection
ParaPass_LV3 = NetPara(:,NetPassIndx_LV3);
% Discard Eleak, Kd2, SKKCa, Tau1&2
RowDelete = [13,12,9,6,2];
ParaPass_LV3(RowDelete,:) = [];
CellPassIndx = reshape(NetPassIndx_LV3,5,[]);
NetIDPass = NetCellID(CellPassIndx);

%% Analyze correlation
plotmatrix(ParaPass_LV3');
[R,~] = corr(ParaPass_LV3','type','Spearman');

% Visualize the COC result
figure('Name','Correlation Coefficients');
imagesc(R);
colormap('default');
colorbar;
set(gca, 'XTick', 1:9); % center x-axis ticks on bins
set(gca, 'YTick', 1:9); % center y-axis ticks on bins
xticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'CaL', 'CAN', 'CaT', 'NaP','G_syn'});
yticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'CaL', 'CAN', 'CaT', 'NaP','G_syn'});

%% Save the Parameters and cell ID  
SavingLoc = '../../lv3-results/LV3ParaPass.txt';
SavePara = ParaPass_LV3;
save(SavingLoc,'SavePara','-ascii');

SavingLoc2 = '../../lv3-results/NetIDPass.txt';
SavePara2 = NetIDPass;
save(SavingLoc,'SavePara2','-ascii');

