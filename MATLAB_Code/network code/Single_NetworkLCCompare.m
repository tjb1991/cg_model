%% This program simply reads in all the output traces and displays them.

NetPre = importdata('..\..\lv3-out\NetPre.txt');
NetPost = importdata('..\..\lv3-out\NetPost.txt');
NetPreSIZ = importdata('..\..\lv3-out\NetPreSIZ.txt');
NetPostSIZ = importdata('..\..\lv3-out\NetPostSIZ.txt');
NetPreNeu = importdata('..\..\lv3-out\NetPreNeu.txt');
NetPostNeu = importdata('..\..\lv3-out\NetPostNeu.txt');

CellPre = importdata('..\..\lv3-out\CellPre.txt');
CellPost = importdata('..\..\lv3-out\CellPost.txt');
CellPreSIZ = importdata('..\..\lv3-out\CellPreSIZ.txt');
CellPostSIZ = importdata('..\..\lv3-out\CellPostSIZ.txt');

NetPreCurrent = importdata('..\..\lv3-out\NetPreCurrent.txt');
NetPostCurrent = importdata('..\..\lv3-out\NetPostCurrent.txt');

NetPreCurrentNSI = importdata('..\..\lv3-out\NetPreCurrentNSI.txt');
NetPostCurrentNSI = importdata('..\..\lv3-out\NetPostCurrentNSI.txt');

NetPreCurrentStim = importdata('..\..\lv3-out\NetPreCurrentStim.txt');
NetPostCurrentStim = importdata('..\..\lv3-out\NetPostCurrentStim.txt');

tstop = 2550;
dt = 0.5;
cut_first_ms = 100;

time_end = (tstop/dt)+1;
sim_run_time = tstop-cut_first_ms;

network = 1;
pointer = 5*(network-1);

NetworkNum = 100;
SCtime = [0:dt:tstop-cut_first_ms-dt]';
%CurtimeOrig = [0:1:12755-2]';
%Curtime = linspace(0,2500,12755-1);
% Cut some of the artifact data off
NetPre(time_end:end,:)= [];
NetPost(time_end:end,:)= [];
NetPreSIZ(time_end:end,:)= [];
NetPostSIZ(time_end:end,:)= [];
NetPreNeu(time_end:end,:)= [];
NetPostNeu(time_end:end,:)= [];

CellPre(time_end:end,:)= [];
CellPost(time_end:end,:)= [];
CellPreSIZ(time_end:end,:)= [];
CellPostSIZ(time_end:end,:)= [];

NetPreCurrent(time_end:end,:)= [];
NetPostCurrent(time_end:end,:)= [];

NetPreCurrentNSI(time_end:end,:)= [];
NetPostCurrentNSI(time_end:end,:)= [];

%bit of a hack job here, size of the array should be the same for
%consistacy
stim_size = ((tstop-cut_first_ms)/dt)+1;
NetPreCurrentStim(stim_size:end,:)= [];
NetPostCurrentStim(stim_size:end,:)= [];
%If we're cutting the first part we should adjust times too
NetPreCurrentStim(NetPreCurrentStim==0) = nan;
NetPostCurrentStim(NetPostCurrentStim==0) = nan;
NetPreCurrentStim = NetPreCurrentStim - cut_first_ms;
NetPostCurrentStim = NetPostCurrentStim - cut_first_ms;

% Discard the first 50 ms data points to avoid V_init effect
NetPre(1:(cut_first_ms/dt),:)= [];
NetPost(1:(cut_first_ms/dt),:)= [];
NetPreSIZ(1:(cut_first_ms/dt),:)= [];
NetPostSIZ(1:(cut_first_ms/dt),:)= [];
NetPreNeu(1:(cut_first_ms/dt),:)= [];
NetPostNeu(1:(cut_first_ms/dt),:)= [];

CellPre(1:(cut_first_ms/dt),:)= [];
CellPost(1:(cut_first_ms/dt),:)= [];
CellPreSIZ(1:(cut_first_ms/dt),:)= [];
CellPostSIZ(1:(cut_first_ms/dt),:)= [];

NetPreCurrent(1:(cut_first_ms/dt),:)= [];
NetPostCurrent(1:(cut_first_ms/dt),:)= [];
NetPreCurrentNSI(1:(cut_first_ms/dt),:)= [];
NetPostCurrentNSI(1:(cut_first_ms/dt),:)= [];

for network = 1:1
pointer = 5*(network-1);
counter = pointer;
for i = 1:NetworkNum%:NetworkNum
    figure
    ax1 = subplot(2,3,1);
    ax2 = subplot(2,3,2);
    ax3 = subplot(2,3,3);
    ax4 = subplot(2,3,4);
    ax5 = subplot(2,3,5);
    plot(ax1,SCtime,NetPre(:,counter+1))
    plot(ax2,SCtime,NetPre(:,counter+2))
    plot(ax3,SCtime,NetPre(:,counter+3))
    plot(ax4,SCtime,NetPre(:,counter+4))
    plot(ax5,SCtime,NetPre(:,counter+5))
    counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
    a = axes;
    t1 = title('NetPre');
    a.Visible = 'off'; % set(a,'Visible','off');
    t1.Visible = 'on'; % set(t1,'Visible','on');
end

counter = pointer;
for i = 1%:NetworkNum
    figure
    ax1 = subplot(2,3,1);
    ax2 = subplot(2,3,2);
    ax3 = subplot(2,3,3);
    ax4 = subplot(2,3,4);
    ax5 = subplot(2,3,5);
    plot(ax1,SCtime,NetPost(:,counter+1))
    plot(ax2,SCtime,NetPost(:,counter+2))
    plot(ax3,SCtime,NetPost(:,counter+3))
    plot(ax4,SCtime,NetPost(:,counter+4))
    plot(ax5,SCtime,NetPost(:,counter+5))
    counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
    ylim(ax1,[-50 10]);
    ylim(ax2,[-50 10]);
    ylim(ax3,[-50 10]);
    ylim(ax4,[-50 10]);
    ylim(ax5,[-50 10]);
    a = axes;
    t1 = title('NetPost');
    a.Visible = 'off'; % set(a,'Visible','off');
    t1.Visible = 'on'; % set(t1,'Visible','on');
end

% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,NetPost(:,counter+1))
%     plot(ax2,SCtime,NetPost(:,counter+2))
%     plot(ax3,SCtime,NetPost(:,counter+3))
%     plot(ax4,SCtime,NetPost(:,counter+4))
%     plot(ax5,SCtime,NetPost(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('NetPost');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,NetPreSIZ(:,counter+1))
%     plot(ax2,SCtime,NetPreSIZ(:,counter+2))
%     plot(ax3,SCtime,NetPreSIZ(:,counter+3))
%     plot(ax4,SCtime,NetPreSIZ(:,counter+4))
%     plot(ax5,SCtime,NetPreSIZ(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('NetPreSIZ');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,NetPostSIZ(:,counter+1))
%     plot(ax2,SCtime,NetPostSIZ(:,counter+2))
%     plot(ax3,SCtime,NetPostSIZ(:,counter+3))
%     plot(ax4,SCtime,NetPostSIZ(:,counter+4))
%     plot(ax5,SCtime,NetPostSIZ(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('NetPostSIZ');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% 
% 
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,CellPre(:,counter+1))
%     plot(ax2,SCtime,CellPre(:,counter+2))
%     plot(ax3,SCtime,CellPre(:,counter+3))
%     plot(ax4,SCtime,CellPre(:,counter+4))
%     plot(ax5,SCtime,CellPre(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('CellPre');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,CellPost(:,counter+1))
%     plot(ax2,SCtime,CellPost(:,counter+2))
%     plot(ax3,SCtime,CellPost(:,counter+3))
%     plot(ax4,SCtime,CellPost(:,counter+4))
%     plot(ax5,SCtime,CellPost(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('CellPost');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,CellPreSIZ(:,counter+1))
%     plot(ax2,SCtime,CellPreSIZ(:,counter+2))
%     plot(ax3,SCtime,CellPreSIZ(:,counter+3))
%     plot(ax4,SCtime,CellPreSIZ(:,counter+4))
%     plot(ax5,SCtime,CellPreSIZ(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('CellPreSIZ');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,SCtime,CellPostSIZ(:,counter+1))
%     plot(ax2,SCtime,CellPostSIZ(:,counter+2))
%     plot(ax3,SCtime,CellPostSIZ(:,counter+3))
%     plot(ax4,SCtime,CellPostSIZ(:,counter+4))
%     plot(ax5,SCtime,CellPostSIZ(:,counter+5))
%     counter = counter + 5;
%     xlim(ax1,[0 2500]);
%     xlim(ax2,[0 2500]);
%     xlim(ax3,[0 2500]);
%     xlim(ax4,[0 2500]);
%     xlim(ax5,[0 2500]);
%     ylim(ax1,[-50 10]);
%     ylim(ax2,[-50 10]);
%     ylim(ax3,[-50 10]);
%     ylim(ax4,[-50 10]);
%     ylim(ax5,[-50 10]);
%     a = axes;
%     t1 = title('CellPostSIZ');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% 
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,Curtime,NetPreCurrent(:,counter+1))
%     plot(ax2,Curtime,NetPreCurrent(:,counter+2))
%     plot(ax3,Curtime,NetPreCurrent(:,counter+3))
%     plot(ax4,Curtime,NetPreCurrent(:,counter+4))
%     plot(ax5,Curtime,NetPreCurrent(:,counter+5))
%     counter = counter + 5;
%     a = axes;
%     t1 = title('NetPreCurrent');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,Curtime,NetPostCurrent(:,counter+1))
%     plot(ax2,Curtime,NetPostCurrent(:,counter+2))
%     plot(ax3,Curtime,NetPostCurrent(:,counter+3))
%     plot(ax4,Curtime,NetPostCurrent(:,counter+4))
%     plot(ax5,Curtime,NetPostCurrent(:,counter+5))
%     counter = counter + 5;
%     a = axes;
%     t1 = title('NetPostCurrent');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,Curtime,NetPreCurrentNSI(:,counter+1))
%     plot(ax2,Curtime,NetPreCurrentNSI(:,counter+2))
%     plot(ax3,Curtime,NetPreCurrentNSI(:,counter+3))
%     plot(ax4,Curtime,NetPreCurrentNSI(:,counter+4))
%     plot(ax5,Curtime,NetPreCurrentNSI(:,counter+5))
%     counter = counter + 5;
%     a = axes;
%     t1 = title('NetPreCurrent SIZ to Nurite');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,Curtime,NetPostCurrentNSI(:,counter+1))
%     plot(ax2,Curtime,NetPostCurrentNSI(:,counter+2))
%     plot(ax3,Curtime,NetPostCurrentNSI(:,counter+3))
%     plot(ax4,Curtime,NetPostCurrentNSI(:,counter+4))
%     plot(ax5,Curtime,NetPostCurrentNSI(:,counter+5))
%     counter = counter + 5;
%     a = axes;
%     t1 = title('NetPostCurrent SIZ to Neurite');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% %Netstim current pretea
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,Curtime,NetPreCurrentStim(:,counter+1))
%     plot(ax2,Curtime,NetPreCurrentStim(:,counter+2))
%     plot(ax3,Curtime,NetPreCurrentStim(:,counter+3))
%     plot(ax4,Curtime,NetPreCurrentStim(:,counter+4))
%     plot(ax5,Curtime,NetPreCurrentStim(:,counter+5))
%     counter = counter + 5;
%     a = axes;
%     t1 = title('NetPreCurrent Stim Input');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end
% 
% %Netstim current posttea
% counter = pointer;
% for i = 1%:NetworkNum
%     figure
%     ax1 = subplot(2,3,1);
%     ax2 = subplot(2,3,2);
%     ax3 = subplot(2,3,3);
%     ax4 = subplot(2,3,4);
%     ax5 = subplot(2,3,5);
%     plot(ax1,Curtime,NetPostCurrentStim(:,counter+1))
%     plot(ax2,Curtime,NetPostCurrentStim(:,counter+2))
%     plot(ax3,Curtime,NetPostCurrentStim(:,counter+3))
%     plot(ax4,Curtime,NetPostCurrentStim(:,counter+4))
%     plot(ax5,Curtime,NetPostCurrentStim(:,counter+5))
%     counter = counter + 5;
%     a = axes;
%     t1 = title('NetPostCurrent Stim Input');
%     a.Visible = 'off'; % set(a,'Visible','off');
%     t1.Visible = 'on'; % set(t1,'Visible','on');
% end

counter = pointer;
NetworkNum = 100
for i=1:NetworkNum
    cell_num = 1;

    h=figure
    ax1 = subplot(1,2,1);
    ax2 = subplot(1,2,2);
    plot(ax1, SCtime,NetPre(:,counter+cell_num))
    plot(ax2, SCtime,NetPost(:,counter+cell_num))
    xlim(ax1,[0 sim_run_time])
    xlim(ax2,[0 sim_run_time])
    ylim(ax1,[-50 0]);
    ylim(ax2,[-50 0]);
    title(ax1,'Pre-TEA')
    title(ax2,'Post-TEA')
    counter = counter + 5;
    %saveas(h, string(i),'jpg')
end

    counter = pointer;
for i = 1:NetworkNum
    h = figure;

    ax1 = subplot(2,3,1);
    scatter(NetPreCurrentStim(:,counter+5),0*ones(size(SCtime)),'x')%5 because stims are only recorded in last
    title(ax1,'Pre-TEA Spike Input')
    xlim([0 sim_run_time])
    ylim([0 10])

    ax2 = subplot(2,3,2);
    plot(SCtime,NetPre(:,counter+cell_num),SCtime,NetPreNeu(:,counter+cell_num),SCtime,NetPreSIZ(:,counter+cell_num))
    ylabel('mV')
    legend('Soma', 'Neurite', 'SIZ')
    title('Pre-TEA Voltage')
    xlim([0 sim_run_time])

    ax3 = subplot(2,3,3);
    yyaxis left
    plot(SCtime,NetPreCurrentNSI(:,counter+cell_num))
    ylabel('nA')
    %ylim([-20 40]);
    yyaxis right
    plot(SCtime,NetPreCurrent(:,counter+cell_num))
    ylabel('nA')
    legend('SIZ->Neurite','Neurite->Soma')
    title('Pre-TEA Current')
    ylim([-.01 .05]);
    xlim([0 sim_run_time])

    ax4 = subplot(2,3,4);
    scatter(NetPostCurrentStim(:,counter+5),0*ones(size(SCtime)),'x')
    title(ax4,'Post-TEA Spike Input')
    xlim([0 sim_run_time])
    ylim([0 10])

    ax5 = subplot(2,3,5);
    plot(SCtime,NetPost(:,counter+cell_num),SCtime,NetPostNeu(:,counter+cell_num),SCtime,NetPostSIZ(:,counter+cell_num))
    ylabel('mV')
    legend('Soma', 'Neurite', 'SIZ')
    title('Post-TEA Voltage')
    xlim([0 sim_run_time])

    ax6 = subplot(2,3,6);
    yyaxis left
    plot(SCtime,NetPostCurrentNSI(:,counter+cell_num))
    ylabel('nA')
    %ylim([-20 40]);
    yyaxis right
    plot(SCtime,NetPostCurrent(:,counter+cell_num))
    ylabel('nA')
    legend('SIZ->Neurite','Neurite->Soma')
    title('Post-TEA Current')
    ylim([-.01 .05]);
    xlim([0 sim_run_time])
    counter = counter + 5;
    %saveas(h, string(i),'jpg')
end

%ax4 = subplot(2,3,4);
%plot(Curtime,NetPreCurrent(:,counter+cell_num))
%ylabel('nA')
%title('Current Neurite->Soma')

%ax5 = subplot(2,3,5);
%plot(SCtime,NetPre(:,counter+cell_num))
%ylabel('mV')
%title('Soma Voltage')

%a = axes;
%a.Visible = 'off'; % set(a,'Visible','off');


% counter = pointer
% figure
% ax1 = subplot(2,3,1);
% ax2 = subplot(2,3,2);
% ax3 = subplot(2,3,3);
% ax4 = subplot(2,3,4);
% ax5 = subplot(2,3,5);
% plot(ax1,Curtime,NetPostCurrentStim(:,counter+cell_num))
% title(ax1,'POST-TEA Current Input')
% plot(ax2,SCtime,NetPostSIZ(:,counter+cell_num))
% ylabel(ax2, 'mV')
% title(ax2,'SIZ Voltage')
% plot(ax3,Curtime,NetPostCurrentNSI(:,counter+cell_num))
% ylabel(ax3, 'nA')
% title(ax3,'Current SIZ->Neurite')
% plot(ax4,Curtime,NetPostCurrent(:,counter+cell_num))
% ylabel(ax4, 'nA')
% title(ax4,'Current Neurite->Soma')
% plot(ax5,SCtime,NetPost(:,counter+cell_num))
% ylabel(ax5, 'mV')
% title(ax5,'Soma Voltage')
% a = axes;
% a.Visible = 'off'; % set(a,'Visible','off');
end