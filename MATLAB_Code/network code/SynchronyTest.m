% This program calculate the synchrony between waveforms by calculating R-squared values
% between voltage traces.
% And then save the network ID that has Synchrony Scores within the
% observed ranges.

for j = 0:NetworkNum-1
    
    NetPreLC3(:,j+1) = NetPre(:,j*5+3);
    NetPreLC4(:,j+1) = NetPre(:,j*5+4);
    NetPreLC5(:,j+1) = NetPre(:,j*5+5);
    
    NetPostLC3(:,j+1) = NetPost(:,j*5+3);
    NetPostLC4(:,j+1) = NetPost(:,j*5+4);
    NetPostLC5(:,j+1) = NetPost(:,j*5+5);   
       
end

for j = 1:NetworkNum
    co = corrcoef(NetPreLC3(:,j),NetPreLC4(:,j));
    RPre34(j) = co(1,2);
    co = corrcoef(NetPreLC4(:,j),NetPreLC5(:,j));
    RPre45(j) = co(1,2);
    co = corrcoef(NetPostLC3(:,j),NetPostLC4(:,j));
    RPost34(j) = co(1,2);
    co = corrcoef(NetPostLC4(:,j),NetPostLC5(:,j));
    RPost45(j) = co(1,2);
end

SyncScorePre34 = RPre34.^2;
SyncScorePre45 = RPre45.^2;
SyncScorePost34 = RPost34.^2;
SyncScorePost45 = RPost45.^2;

SyncPre34Pass= double.empty(1,0);
for i = 1:length(SyncScorePre34)
    if SyncScorePre34(i)>=0.92&&SyncScorePre34(i)<=1
        SyncPre34Pass(end+1)=i;
    end
end

SyncPost34Pass= double.empty(1,0);
for i = 1:length(SyncScorePost34)
    if SyncScorePost34(i)>0&&SyncScorePost34(i)<0.92
        SyncPost34Pass(end+1)=i;
    end
end

SyncPre45Pass= double.empty(1,0);
for i = 1:length(SyncScorePre45)
    if SyncScorePre45(i)>=0.98&&SyncScorePre45(i)<=1
        SyncPre45Pass(end+1)=i;
    end
end

SyncPost45Pass= double.empty(1,0);
for i = 1:length(SyncScorePost45)
    if SyncScorePost45(i)>=0.95&&SyncScorePost45(i)<=1
        SyncPost45Pass(end+1)=i;
    end
end

S1 = intersect(SyncPre34Pass,SyncPost34Pass);
S2 = intersect(SyncPre45Pass,SyncPost45Pass);
SyncPass = intersect(S1,S2);

for j = 1:length(SyncPass)
  
    for i = 1:5
        SyncPassCellID(i+(j-1)*5) = SyncPass(j)*5+i;
    end
end
