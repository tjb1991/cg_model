
function [LCBD,Latency] = BurstDurMeasure(x, y, peak_threshold, spike_width)
    yg = y > peak_threshold; %find values above threshold
    %apply convolution to find values that meet the threshold and their surrounding points
    sumX = conv2(yg,ones(spike_width),'same');
    %find the indexes that aren't zero
    idx = sumX ~= 0;
    
    %Calculate LC Burst Duration
    SpikeTime = x(idx);
    if isempty(SpikeTime)
        SpikeTime = 300;
    end
    LCBD = SpikeTime(end)-SpikeTime(1); % in ms
    
    %Calculate Latency
    
    Latency = SpikeTime(1) - 300; % in ms
    
    %interpolation over the NaN values
    %y(isnan(y)) = interp1(find(~isnan(y)), y(~isnan(y)), find(isnan(y)),'PCHIP'); 
end