% This program tries to find the model waveform that are close to a biology
% one.
SpikeFind = double.empty(1,0);
VpeakFind = double.empty(1,0);

for j = 1:NetworkNum*5 
    if SpikeNum(j)==6
        SpikeFind(end+1) = j;  
    end
    
    if Vpeak(j)>=19&&Vpeak(j)<=20
        VpeakFind(end+1) = j;  
    end
end

netfind = intersect(SpikeFind,VpeakFind,'stable');

picker = 366*5+3;
figure('Name','NetPre')
plot(SCtime,NetPre(:,picker));
figure('Name','NetPreSIZ')
plot(SCtime,NetPreSIZ(:,picker));
figure('Name','NetPost')
plot(SCtime,NetPost(:,picker));
figure('Name','NetPostSIZ')
plot(SCtime,NetPostSIZ(:,picker));



format = ' RMP= %f\n Vpeak= %f\n SpikeN= %d\n LCBD= %f\n Latency= %f\n Base= %f\n Area= %f\n Peak= %f\n';
fprintf(format,RMP(picker),Vpeak(picker),SpikeNum(picker),LCBD(picker),Latency(picker),...
    Baseline_Pre_LV3(1,picker),AreaPre_LV3(1,picker),PeakPre_LV3(1,picker))

