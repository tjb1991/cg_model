% This program randomly pick a cell along with the measured properties to
% examine whether the tools are functioning properly at LV3.

picker = random('unid',NetworkNum*5);
figure('Name','NetPre')
plot(SCtime,NetPre(:,picker));
figure('Name','NetPreSIZ')
plot(SCtime,NetPreSIZ(:,picker));

format = ' RMP= %f\n Vpeak= %f\n SpikeN= %d\n LCBD= %f\n Latency= %f\n Base= %f\n Area= %f\n Peak= %f\n';
fprintf(format,RMP(picker),Vpeak(picker),SpikeNum(picker),LCBD(picker),Latency(picker),...
    Baseline_Pre_LV3(1,picker),AreaPre_LV3(1,picker),PeakPre_LV3(1,picker))
