Stim = importdata('..\Stimulus_Protocol_Sources\stim1.txt');

figure
dt = .5
plot(Stim(1:25500*dt,1)*1000,Stim(1:25500*dt,2))
title('Stimulus Protocol 1')
xlim([0 2550])