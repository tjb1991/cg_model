% This program generates plots that evaluate the performance of the models
% and the rejection procedure.

%% Create Rejection rate vs property plot

% 1. count models that pass each property test and store the number in a vector.
PassCount(1) = length(APrePass_LV2);
PassCount(2) = length(PPrePass_LV2);
PassCount(3) = length(BPrePass_LV2);
PassCount(4) = length(APostPass_LV2);
PassCount(5) = length(BPostPass_LV2);
PassCount(6) = length(WavePassIndx_LV2);
PassCount(7) = length(PassIndxLV_2);

% 2. plot the vector.
figure('Name','Pass rate vs property')
bar(PassCount);
xticklabels({'AreaPre','PeakPre','BaselinePre','AreaPost','BaselinePost','Wave','Overall'})
ylabel('Number of models that pass')
xlabel('Properties');

%% Examine the shape of post-rejection parameter space

% Change the source of GRangeBio to Para_LV2 if the goals is to compare
% pre- and post-selection size
h = Para_LV2(1:11,:);
h(end+1,:)=Para_LV2(14,:);    %netcon weight
h(end+1,:)=Para_LV2(16,:);    %freq
h(2,:) = []; % discard Eleak

for i = 1:12
    GRangePre(:,i)= [min(h(i,:)) max(h(i,:))];
end

GRangePre(:,1:4) = GRangePre(:,1:4).*10^4;
GRangePre(:,5:10) = GRangePre(:,5:10).*10^5;
GRangePre(:,8) = GRangePre(:,8)./10;
GRangePre(:,11) = GRangePre(:,11).*100;

% 1. store the original max and min G in a vector
GRangeBio(:,1)= [0.62 9.7];  %e-4
GRangeBio(:,2)= [1.72 19]; %e-4
GRangeBio(:,3)= [7.9 61];   %e-4
GRangeBio(:,4)= [1.65 12.7]; %e-4
GRangeBio(:,5)= [9.1 50];   %e-5
GRangeBio(:,6)= [6.5 13];   %e-5
GRangeBio(:,7)= [7 15];     %e-5   
GRangeBio(:,8)= [8.8 20];   %e-4
GRangeBio(:,9)= [16 31];    %e-5
GRangeBio(:,10)= [7.6 35];  %e-5
GRangeBio(:,11)= [1 8];  %e-2, NetCon weight
GRangeBio(:,12)= [15 32]; %Hz, SC freq

% 2. store the post-rejection max and min G in a vector
g = ParaPassLV_2(1:11,:);
g(end+1,:)=ParaPassLV_2(14,:);    %netcon weight
g(end+1,:)=ParaPassLV_2(16,:);    %freq
g(2,:) = []; % discard Eleak

for i = 1:12
    GRangePost(:,i)= [min(g(i,:)) max(g(i,:))];
end

GRangePost(:,1:4) = GRangePost(:,1:4).*10^4;
GRangePost(:,5:10) = GRangePost(:,5:10).*10^5;
GRangePost(:,8) = GRangePost(:,8)./10;
GRangePost(:,11) = GRangePost(:,11).*100;

% 3. plot them 
figure('Name','Space shape comparison')
plot(GRangePre(2,:),'v','MarkerEdgeColor','k','MarkerFaceColor','k');
hold on
plot(GRangePre(1,:),'^','MarkerEdgeColor','k','MarkerFaceColor','k');
plot(GRangeBio(2,:),'v','MarkerEdgeColor','b');
hold on
plot(GRangeBio(1,:),'^','MarkerEdgeColor','b');
plot(GRangePost(2,:),'v','MarkerEdgeColor','r');
plot(GRangePost(1,:),'^','MarkerEdgeColor','r');
xticks([1:12]);
xticklabels({'Leak*e-4','A*e-4','BK*e-4','Kd1*e-4','Kd2*e-5','CaS*e-5','CAN*e-5','SK*e-4','CaT*e-5','NaP*e-5','NetCon Weight','SC Freq'})
ylabel('G (S/cm2) (Hz for freq)')
legend('PreSelect max','PreSelect min','Bio max','Bio min','PostSelect max','PostSelect min')

% 4. (Optional) put in models to compare
GBad = Para_LV1(1:11,av);
GBad(2,:) = [];
GGood = Para_LV1(1:11,ainv);
GGood(2,:) = [];
%Scale
GBad(1:4,:) = GBad(1:4,:).*10^4;
GBad(5:10,:) = GBad(5:10,:).*10^5;
GBad(8,:) = GBad(8,:)./10;
GGood(1:4,:) = GGood(1:4,:).*10^4;
GGood(5:10,:) = GGood(5:10,:).*10^5;
GGood(8,:) = GGood(8,:)./10;
plot(GGood,'o','MarkerEdgeColor','g','MarkerFaceColor','g');
plot(GBad,'o','MarkerEdgeColor','k');

%% Select some typical valid models and compare with the invalid ones  
% (need some manual selection and can adjust based on the result)

% 1. compare the model(s) with the (or almost) largest valid post-TEA area
am = find(AreaPost_LV2(1,:)>=max(AreaPost_LV2(1,PassIndxLV_2))-900);
av = intersect(am,PassIndxLV_2); % This will give the valid models
ainv = setxor(am,av); % This will give the models not in intersect = invalid

% 2. Pick several of them and plot (can adjust manually)
figure('Name','Valid Models With Max APost')
plot(subplot(1,2,1),SCtime,SynPre_LV2(:,av(2)));
ylim([-50 10]);
plot(subplot(1,2,2),SCtime,SynPost_LV2(:,av(2)));
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

figure('Name','Invalid Models With Max APost')
plot(subplot(1,2,1),SCtime,SynPre_LV2(:,1466));
ylim([-60 20]);
plot(subplot(1,2,2),SCtime,SynPost_LV2(:,1466));
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')
% 3.(Optional) compare the model(s) with the (or almost) largest valid pre-TEA Vpeak
pm = find(PeakPre_LV2(1,:)>=max(PeakPre_LV2(1,PassIndxLV_2))-2);
pv = intersect(pm,PassIndxLV_2); % This will give the valid models
pinv = setxor(pm,pv); % This will give the models not in intersect = invalid

% 4.(Optional) Pick several of them and plot (can adjust manually)
figure('Name','Valid Models With Max PPre')
plot(subplot(1,2,1),SCtime,SynPre_LV2(:,pv(2)));
ylim([-50 0]);
plot(subplot(1,2,2),SCtime,SynPost_LV2(:,pv(2)));
ylim([-50 0]);
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

figure('Name','Invalid Models With Max PPre')
plot(subplot(1,2,1),SCtime,SynPre_LV2(:,pinv(1)));
ylim([-60 10]);
plot(subplot(1,2,2),SCtime,SynPost_LV2(:,pinv(1)));
ylim([-60 10]);
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

% 5. other models
FailIndx = setxor(PassIndxLV_2,[1:Trials2]);

% 6. Random selection from the passed models
for n = 1:282
picker = PassIndxLV_2(n)%random('unid',length(PassIndxLV_2)));
h = figure()
plot(subplot(1,2,1),SCtime,SynPre_LV2(:,picker));
title(subplot(1,2,1),"PreTEA Soma cell #" + picker)
ylim([-50 0]);
plot(subplot(1,2,2),SCtime,SynPost_LV2(:,picker));
title(subplot(1,2,2),"PostTEA Soma cell #" + picker)
ylim([-50 0]);
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')
saveas(h, string(n),'jpg')
close
end

picker = FailIndx(random('unid',length(FailIndx)));
figure()
plot(subplot(1,2,1),SCtime,SynPre_LV2(:,picker));
ylim([-50 0]);
plot(subplot(1,2,2),SCtime,SynPost_LV2(:,picker));
ylim([-50 0]);
ylabel(subplot(1,2,1),'Vm (mV)')
xlabel(subplot(1,2,1),'Time(ms)')
xlabel(subplot(1,2,2),'Time(ms)')

% 7. Plot all the models (not recommend)

for i = 1:length(pv)
    figure()
    plot(subplot(1,2,1),SCtime,SynPre_LV2(:,pv(i)));
    ylim([-67 0]);
    plot(subplot(1,2,2),SCtime,SynPost_LV2(:,pv(i)));
    ylim([-67 0]);
    ylabel(subplot(1,2,1),'Vm (mV)')
    xlabel(subplot(1,2,1),'Time(ms)')
    xlabel(subplot(1,2,2),'Time(ms)')
end

for i = 1:length(pinv)
    figure()
    plot(subplot(1,2,1),SCtime,SynPre_LV2(:,pinv(i)));
    ylim([-67 0]);
    plot(subplot(1,2,2),SCtime,SynPost_LV2(:,pinv(i)));
    ylim([-67 0]);
    ylabel(subplot(1,2,1),'Vm (mV)')
    xlabel(subplot(1,2,1),'Time(ms)')
    xlabel(subplot(1,2,2),'Time(ms)')
end

%% Examine the correlation between each pair of G
% 1. Search for possible correlations between parameters
ParaCorLV_2 = ParaPassLV_2(1:11,:)';    % The corr function compares column with column
ParaCorLV_2(:,2)=[];            % Discard ELeak

[R2,~] = corr(ParaCorLV_2,'type','Spearman');

% 2. Visualize the COC result
figure('Name','Matrix Plot')
plotmatrix(ParaCorLV_2);

figure('Name','Correlation Coefficients')
imagesc(abs(R2));
colormap('default');
colorbar;
set(gca, 'XTick', 1:10); % center x-axis ticks on bins
set(gca, 'YTick', 1:10); % center y-axis ticks on bins
xticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'Kd2', 'CaL', 'CAN', 'SKKCa', 'CaT', 'NaP'});
yticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'Kd2', 'CaL', 'CAN', 'SKKCa', 'CaT', 'NaP'});



