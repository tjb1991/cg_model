%This program performs the LV2 rejection (Only pre- and post-TEA).


Trials2 = 84%624; % Equals to the total number of cells generated
Para_LV2 = importdata('..\lv2-out\IntactParameter.txt');
SynPre_LV2 = importdata('..\lv2-out\SCExc_Pre.txt');
SynPost_LV2 = importdata('..\lv2-out\SCExc_Post.txt');
% SIZ traces are for monitoring not for rejection
SynPreSIZ_LV2 = importdata('..\lv2-out\SCExc_PreSIZ.txt');
SynPostSIZ_LV2 = importdata('..\lv2-out\SCExc_PostSIZ.txt');

%The order of parameters:
% Leak_G 
% Leak_E 
% A 
% BKKCa 
% Kd1 
% Kd2
% CaL 
% CAN 
% SKKCa 
% CaT 
% NaP
% Tau1
% Tau2
% NetCon weight
% Noise level
% SC spiking freq
% Vrest
% Rin 
% Tau

%% Tag the cells
Para_LV2(end+1,:)=1:Trials2;

% We don't reject passive properties at LV2
%% Calculate the Pre-TEA Vpeak and Area
SCtime = [0:0.5:2500-0.5]';

% Discard the first 50 ms data points to avoid V_init effect
SynPre_LV2(1:(50/0.5),:) = [];
SynPost_LV2(1:(50/0.5),:) = [];
SynPreSIZ_LV2(1:(50/0.5),:) = [];
SynPostSIZ_LV2(1:(50/0.5),:) = [];

% Get rid of the last point (which jumps to 0)
SynPre_LV2(end,:) = [];
SynPost_LV2(end,:) = [];
SynPreSIZ_LV2(end,:) = [];
SynPostSIZ_LV2(end,:) = [];

for i = 1:length(SynPre_LV2(1,:))
    Baseline_Pre_LV2(1,i)= mean(SynPre_LV2(SCtime<=100,i));
    AreaPre_LV2(1,i) = abs(trapz(SCtime,(SynPre_LV2(:,i)-Baseline_Pre_LV2(1,i))));
    PeakPre_LV2(1,i) = max(SynPre_LV2(:,i))-Baseline_Pre_LV2(1,i);
    % Tag the cells
    Baseline_Pre_LV2(2,i)= Para_LV2(end,i);
    AreaPre_LV2(2,i)= Para_LV2(end,i);
    PeakPre_LV2(2,i)= Para_LV2(end,i);
end

% Reject based on Pre-TEA response (Area under the curve)
APrePass_LV2 = double.empty(1,0);
for i = 1:length(AreaPre_LV2(1,:))
    if AreaPre_LV2(1,i)>=4640&&AreaPre_LV2(1,i)<=18373
       APrePass_LV2(end+1)=AreaPre_LV2(2,i);
    end
end

% Reject based on Pre-TEA response (maximum dV above baseline)
PPrePass_LV2 = double.empty(1,0);
for i = 1:length(PeakPre_LV2(1,:))
    if PeakPre_LV2(1,i)>=10.03&&PeakPre_LV2(1,i)<=33.76
        PPrePass_LV2(end+1)=PeakPre_LV2(2,i);
    end
end

%% Reject by Baseline_pre (avoid overshadowing)
BPrePass_LV2 = double.empty(1,0);
for i = 1:length(Baseline_Pre_LV2(1,:))
    if Baseline_Pre_LV2(1,i)>=Para_LV2(17,i)-3&&Baseline_Pre_LV2(1,i)<=Para_LV2(17,i)+3
        BPrePass_LV2(end+1)=Baseline_Pre_LV2(2,i);
    end
end
%% Calculate Post-TEA Vpeak and Area

% Baseline for burst No.1 is chosen from V(t=0~0.1s)

for i = 1:length(SynPost_LV2(1,:))
    Baseline_Post_LV2(1,i)= mean(SynPost_LV2(SCtime<=100,i));
    AreaPost_LV2(1,i) = abs(trapz(SCtime,(SynPost_LV2(:,i)-Baseline_Post_LV2(1,i)))); 
    PeakPost_LV2(1,i) = max(SynPost_LV2(:,i))-Baseline_Post_LV2(1,i);
    % Tag the cells
    Baseline_Post_LV2(2,i)= Para_LV2(end,i);  
    AreaPost_LV2(2,i)= Para_LV2(end,i);
    PeakPost_LV2(2,i)= Para_LV2(end,i);
end

%% Reject by Baseline_post (avoid overshadowing)
BPostPass_LV2 = double.empty(1,0);
for i = 1:length(Baseline_Post_LV2(1,:))
    if Baseline_Post_LV2(1,i)>=Para_LV2(17,i)-3&&Baseline_Post_LV2(1,i)<=Para_LV2(17,i)+3
        BPostPass_LV2(end+1)=Baseline_Post_LV2(2,i);
    end
end


%% Reject based on Post-TEA response (Area under the curve)
APostPass_LV2= double.empty(1,0);
for i = 1:length(AreaPost_LV2(1,:))
    if AreaPost_LV2(1,i)>=5178&&AreaPost_LV2(1,i)<=36853
        APostPass_LV2(end+1)=AreaPost_LV2(2,i);
    end
end

%% Reject based on Post-TEA response (maximum dV above baseline)(Not using)
% ID_PPostFailed = double.empty(1,0);
% for i = 1:length(PeakPost(1,:))
%     if PeakPost(1,i)<11.67||PeakPost(1,i)>39.36
%         ID_PPostFailed(end+1)=i;
%     end
% end

%% Pick out the cells that pass all waveform rejections
C21 = intersect(BPostPass_LV2,BPrePass_LV2);
C22 = intersect(C21,APrePass_LV2,'stable');
C23 = intersect(C22,PPrePass_LV2,'stable');
WavePassIndx_LV2 = intersect(C23,APostPass_LV2,'stable');

% Pick out the cells that pass LV1
PassIndxLV_2 = WavePassIndx_LV2;
ParaPassLV_2 = Para_LV2(:,PassIndxLV_2);

%% Save the cell index at each stage
fileID = fopen('../lv3-in/PassCellIndx_LV2.txt','w');
fprintf(fileID,'APrePass_LV2 ');
fprintf(fileID,'%d ',APrePass_LV2);
fprintf(fileID,'\n');
fprintf(fileID,'PPrePass_LV2 ');
fprintf(fileID,'%d ',PPrePass_LV2);
fprintf(fileID,'\n');
fprintf(fileID,'BPrePass_LV2 ');
fprintf(fileID,'%d ',BPrePass_LV2);
fprintf(fileID,'\n');
fprintf(fileID,'APostPass_LV2 ');
fprintf(fileID,'%d ',APostPass_LV2);
fprintf(fileID,'\n');
fprintf(fileID,'BPostPass_LV2 ');
fprintf(fileID,'%d ',BPostPass_LV2);
fprintf(fileID,'\n');
fprintf(fileID,'WavePassIndx_LV2 ');
fprintf(fileID,'%d ',WavePassIndx_LV2);
fprintf(fileID,'\n');
fprintf(fileID,'PassIndxLV_2 ');
fprintf(fileID,'%d ',PassIndxLV_2);
fprintf(fileID,'\n');
fclose(fileID);

%% Save the Parameters into a new file for LV2-Intact cell generation
SavingLoc = '../lv3-in/LV3ParaInput.txt';
SavePara = ParaPassLV_2(1:16,:);
save(SavingLoc,'SavePara','-ascii','-double');

size(ParaPassLV_2)
%{
%% Search for possible correlations between parameters
Para_z = zscore(ParaPassLV_2(1:11,:)');
Para_z(:,2) = [];    % Discard ELeak
[R1,P1] = corrcoef(Para_z(:,1:10));
% plotmatrix(Para_z2(:,1:14));

%% Visualize the COC result
figure('Name','Correlation Coefficients');
imagesc(abs(R1));
colormap('default');
colorbar;
set(gca, 'XTick', 1:10); % center x-axis ticks on bins
set(gca, 'YTick', 1:10); % center y-axis ticks on bins
xticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'Kd2', 'CaL', 'CAN', 'SKKCa', 'CaT', 'NaP'});
yticklabels({'Leak_G', 'A', 'BKKCa', 'Kd1', 'Kd2', 'CaL', 'CAN', 'SKKCa', 'CaT', 'NaP'});
%}
