clear

bio_data = '..\..\..\CG_Daniel\TEA\TEA\';

bio270pre = abfload(strcat(bio_data,'270\023_270_0003.abf'));%'023_270_0003_PRE.abf');
bio270post = abfload(strcat(bio_data,'270\023_270_0012.abf'));%'023_270_0012_POST.abf');
bio272pre = abfload(strcat(bio_data,'272\023_272_0000.abf'));
bio272post = abfload(strcat(bio_data,'272\023_272_0012.abf'));

bio267pre = abfload(strcat(bio_data,'267\023_267_0004.abf'));
bio267post = abfload(strcat(bio_data,'267\023_267_0012.abf'));

NetPre = importdata('..\..\lv3-out\NetPre.txt');
NetPost = importdata('..\..\lv3-out\NetPost.txt');
NetPreCurrent = importdata('..\..\lv3-out\NetPreCurrent.txt');
NetPostCurrent = importdata('..\..\lv3-out\NetPostCurrent.txt');


tstop = 2550;
dt = 0.5;
cut_first_ms = 0;
time_end = (tstop/dt)+1;
sim_run_time = tstop-cut_first_ms;
SCtime = [0:dt:tstop-cut_first_ms-dt]';
networks = 100;
cell_num = 1;
% Cut some of the artifact data off
NetPre(time_end:end,:)= [];
NetPost(time_end:end,:)= [];
NetPreCurrent(time_end:end,:)= [];
NetPostCurrent(time_end:end,:)= [];
% Discard the first 50 ms data points to avoid V_init effect
NetPre(1:(cut_first_ms/dt),:)= [];
NetPost(1:(cut_first_ms/dt),:)= [];

for n = 1:1
    h = figure
    pointer = 5*(n-1);

    dim = [.25 .99 0 0];
    str = 'Biology';
    %annotation('textbox',dim,'String',str,'FitBoxToText','on','color','blue');

    dim = [.66 .99 0 0];
    str = 'Model';
    %annotation('textbox',dim,'String',str,'FitBoxToText','on','color','red');

    baseline_voltage = bio270pre(15000,1);
    post_voltage_baseline = bio270pre(15000,1);
    offset = post_voltage_baseline - baseline_voltage;
    
    ax1 = subplot(4,1,1);
    plot(ax1, bio270pre([15000:10:45000],1)-offset);hold on
    %plot(SCtime+3000,NetPre(:,pointer+cell_num)); hold off 
    ylim([-50,0]);
    xlim([0,3000]);
    %title(ax1,'Pre-TEA');
    ylabel('mV')
    dim = [.45 .9 0 0];
    str = 'Control';
    %annotation('textbox',dim,'String',str,'FitBoxToText','on','color','black');
    title("Control")
    lg = legend({'Biology','Model'},'Position',[0.42 0.95 0.01 0.0])
    lg.Box = 'off'
    
    
    %ax1 = subplot(4,2,2);
    %plot(SCtime,NetPostCurrent(:,pointer+cell_num))
    %ylabel('nA');
    %xlim([0,tstop]);
    %title("Model Neurite->Soma Current")
    
    %TEA RESPONSE 1
    baseline_voltage = bio270pre(15000,1);
    post_voltage_baseline = bio270post(25000,1);
    offset = post_voltage_baseline - baseline_voltage;
    
    ax2 = subplot(4,1,2);
    plot(ax2, bio270post([25000:10:55000],1)-offset);hold on
    %plot(SCtime+3000,NetPost(:,pointer+cell_num)); hold off
    ylim([-50,0]);
    xlim([0,3000]);
    ylabel('mV')
    xlabel('time (ms)')
    dim = [.45 .69 0 0];
    str = 'Post TEA 270';
    %annotation('textbox',dim,'String',str,'FitBoxToText','on','color','black');
    title(str)
    %legend('Biology','Model')
    
    %ax1 = subplot(4,2,4);
    %plot(SCtime,NetPostCurrent(:,pointer+cell_num))
    %ylabel('nA');
    %xlim([0,tstop]);
    %title("Control Neurite->Soma Current")
    
    
    %TEA RESPONSE 2
    baseline_voltage = bio272pre(500,1)
    post_voltage_baseline = bio272post(430000,1)
    offset = post_voltage_baseline - baseline_voltage;
    
    ax2 = subplot(4,1,3);
    plot(ax2, bio272post([430000:10:460000],1)-offset);hold on
    %plot(SCtime+3000,NetPost(:,pointer+cell_num)); hold off
    ylim([-55,0]);
    xlim([0,3000]);
    ylabel('mV')
    xlabel('time (ms)')
    dim = [.45 .47 0 0];
    str = 'Post TEA 272';
    %annotation('textbox',dim,'String',str,'FitBoxToText','on','color','black');
    title(str)
    %legend('Biology','Model')
    
    %ax1 = subplot(4,2,6);
    %plot(SCtime,NetPostCurrent(:,pointer+cell_num))
    %ylabel('nA');
    %xlim([0,tstop]);
    %title("Control Neurite->Soma Current")
    
    %TEA RESPONSE 3
    
    baseline_voltage = bio267pre(320000,3)
    post_voltage_baseline = bio267post(2180000,3)
    offset = post_voltage_baseline - baseline_voltage;
    
    ax2 = subplot(4,1,4);
    plot(ax2, bio267post([2180000:10:2210000],3)-offset);hold on %THIS Was on a different channel note 3
    %plot(SCtime+3000,NetPost(:,pointer+cell_num)); hold off
    ylim([-55,0]);
    xlim([0,3000]);
    ylabel('mV')
    xlabel('time (ms)')
    dim = [.45 .24 0 0];
    str = 'Post TEA 267';
    %annotation('textbox',dim,'String',str,'FitBoxToText','on','color','black');
    title(str)
    %legend('Biology','Model')
    
    %ax1 = subplot(4,2,8);
    %plot(SCtime,NetPostCurrent(:,pointer+cell_num))
    %ylabel('nA');
    %xlim([0,tstop]);
    %title("Control Neurite->Soma Current")
    %saveas(h, string(n),'jpg')
end

