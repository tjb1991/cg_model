#!/bin/bash

#SBATCH --job-name=[RSIntact.%j]
#SBATCH -o RSIntact.o%j.txt
#SBATCH -e RSIntact.e%j.txt
#SBATCH -p Lewis
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem-per-cpu 30G
#SBATCH --time 0-23:00
#SBATCH --licenses=matlab:1

module load matlab/matlab-R2018a
module load intel/intel-2016-update2
module load nrn/nrn-mpi-7.4
module load openmpi/openmpi-2.0.0

module list

nrnivmodl

srun nrniv -mpi RST_MakeCells_Soma.hoc

matlab -nodisplay -nojvm -nosplash -r "run('MATLAB Code/RejectionProtocol_LV1.m'),quit"

srun nrniv -mpi RST_MakeCells_Network.hoc