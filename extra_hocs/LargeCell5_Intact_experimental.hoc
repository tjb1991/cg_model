// This template is made for ligated soma Rejection Sampling experiments
// Neurite only has leak currents
// SIZ is using the minimum configuration, i.e. only contains spiking currents


begintemplate LargeCell
	public soma, siz, neu1, synlist, all, connect2target, synapses, type
	public x, y, vcl
	create soma
	create siz
	create neu1									// make sections

	objref synlist, all, syn_, vcl


proc init() {

	strdef type
	type = "LC"
	
	x = 0
	y = 0

	synlist = new List()
	

// CONNECT COMPARTMENTS
	connect soma(1), neu1(0)
	connect neu1(1), siz(0)

	
soma {
		// INSERTING MECHANISMS FROM MOD FILES
			insert leak
			insert a2
			insert bkkca
			insert skkca
			insert kd2
			insert cal
			insert cat
			insert Nn 
			insert nap2
			insert pool		
					
		
		// DEFINE MORPHOLOGY	
			L= 120		       // micron
			diam= 90		       // micron
			cm = 1.5		       // uF/cm2
			Ra = 50		       // ohm-cm
			nseg=1


		// SETTING REVERSAL POTENTIALS FOR INSERTED MECHANISMS
			ek = -80			
			e_leak = -51 		
			ena = 55			
			eca = 45
			enn = -30
			
		// SETTING CONDUCTANCES (AND MORE) FOR INSERTED MECHANISMS (S/cm^2)
	
			g_leak = 1.5e-4 
			g_a2 =  0.0025			
			g_bkkca = 0.011
			gbar_skkca = 8.79e-4	    
			g1_kd2 = 9e-4   		
			g2_kd2 = 9e-5    		
			g_cal = 0.00013	 	
			g_cat = 0.00031			
			gbar_Nn = 1.05e-4				 
			g_nap2 = 1.9e-4
			q_nap2 = 0        				
			f_pool = 20e6  			
			
			
}
		
neu1{ 
		// INSERT MECHANISIMS FROM MOD FILES
			insert leak
			//insert cat
			//insert kd2
			//insert pool	
			insert casLC
			insert nap2
			
		// DEFINE MORPHOLOGY
			L = 1380 			//um
			diam = 20 				//um
			cm = 1.5 			//uF/cm^2    
			Ra = 50		//ohm-cm
			nseg = 1
		
		// SET REVERSAL POTENTIALS
			//ek = -80
			e_leak = -51 		
		
		// SET CONDUCTANCES (S/cm^2)
			g_leak = 1.5e-4
			//g_cat = 0.00031
			//g1_kd2 = 9e-4   		
			//g2_kd2 = 9e-5   				
			//f_pool = 20e6 
			g_nap2 = 0//1.9e-4
			q_nap2 = 0 			

}
		
		
siz{		// model identically to soma
		// INSERT MECHANISIMS FROM MOD FILES
			insert leak
			insert nasiz
			insert kdsiz
			insert casLC
			insert nap2

		
		// DEFINE MORPHOLOGY
			L= 108		// micron
			diam= 20			// micron
			cm = 1.5			// uF/cm2
			Ra = 50 			//ohm-cm
			nseg=1    
		
		// SET REVERSAL POTENTIALS
			ek = -80			
			e_leak = -51			
			ena = 55			

		
		// SET CONDUCTANCES
			g_leak = 1.5e-4 
			g_nasiz = 0.5 
			g_kdsiz = 0. 
			//g_nap2 = 0//1.9e-4
			q_nap2 = 0 	
}


all = new SectionList()
soma all.append()
neu1 all.append()
siz all.append()
		}
		
obfunc connect2target() { localobj nc
	soma nc = new NetCon(&v(1), $o1)
	nc.threshold = 0
	if (numarg() == 2) { $o2 = nc }
	return nc
}

endtemplate LargeCell

/*
begintemplate LargeCell2
	public soma, siz, neu1, axon, synlist, all, connect2target, synapses, type
	public x, y, vcl
	create soma
	create siz
    create axon
	create neu1									// make sections

	objref synlist, all, syn_, vcl


proc init() {

	strdef type
	type = "LC"
	
	x = 0
	y = 0

	synlist = new List()
	

// CONNECT COMPARTMENTS
	connect soma(1), neu1(0)
	connect neu1(1), siz(0)
    connect siz(1),axon(0)

	
soma {
		// INSERTING MECHANISMS FROM MOD FILES
			insert leak
			insert a2
			insert bkkca
			insert skkca
			insert kd2
			insert cal
			insert cat
			insert Nn 
			insert nap2
			insert pool		
					
		
		// DEFINE MORPHOLOGY	
			L= 120		       // micron
			diam= 90		       // micron
			cm = 1.5		       // uF/cm2
			Ra = 50		       // ohm-cm
			nseg=1


		// SETTING REVERSAL POTENTIALS FOR INSERTED MECHANISMS
			ek = -80			
			e_leak = -51 		
			ena = 55			
			eca = 45
			enn = -30
			
		// SETTING CONDUCTANCES (AND MORE) FOR INSERTED MECHANISMS (S/cm^2)
	
			g_leak = 1.5e-4 
			g_a2 =  0.0025			
			g_bkkca = 0.011
			gbar_skkca = 8.79e-4	    
			g1_kd2 = 9e-4   		
			g2_kd2 = 9e-5    		
			g_cal = 0.00013	 	
			g_cat = 0.00031			
			gbar_Nn = 1.05e-4				 
			g_nap2 = 1.9e-4
			q_nap2 = 0        				
			f_pool = 20e6  			
			
			
}
		
neu1{ 
		// INSERT MECHANISIMS FROM MOD FILES
			insert leak
			//insert cat
			//insert kd2
			//insert pool	
			insert casLC
			insert nap2
			
		// DEFINE MORPHOLOGY
			L = 1380 			//um
			diam = 20 				//um
			cm = 1.5 			//uF/cm^2    
			Ra = 50		//ohm-cm
			nseg = 1
		
		// SET REVERSAL POTENTIALS
			//ek = -80
			e_leak = -51 		
		
		// SET CONDUCTANCES (S/cm^2)
			g_leak = 1.5e-4
			//g_cat = 0.00031
			//g1_kd2 = 9e-4   		
			//g2_kd2 = 9e-5   				
			//f_pool = 20e6 
			g_nap2 = 0//1.9e-4
			q_nap2 = 0 			

}
		
		
siz{		// model identically to soma
		// INSERT MECHANISIMS FROM MOD FILES
			//insert leak
			//insert nasiz
			//insert kdsiz
			//insert casLC
			//insert nap2
            
            insert leak
			insert catax
			insert casax
			insert kaax
			insert kcaax
			insert capoolax

		
		// DEFINE MORPHOLOGY
			L= 108		// micron
			diam= 20			// micron
			cm = 1.5			// uF/cm2
			Ra = 50 			//ohm-cm
			nseg=1    
		
		// SET REVERSAL POTENTIALS
			//ek = -80			
			//e_leak = -51			
			//ena = 55			

		
		// SET CONDUCTANCES
			//g_leak = 1.5e-4 
			//g_nasiz = 0.5 
			//g_kdsiz = 0. 
			//g_nap2 = 0//1.9e-4
			//q_nap2 = 0 	
            
            e_leak = -50 // (mV)
			g_leak = .03e-3 * 0.648 // (siemens/cm2)
			
			cao = 3 
			cai = 50e-6 
			gbar_catax = .01 * 0.648 //(.005~.01 siemens/cm2)
			gbar_casax = 0.00684 //.01 * 0.648//(.001~.005 siemens/cm2)
			
			
			gbar_kaax = .2 * 0.648 // (.1~.5 siemens/cm2)
			gbar_kcaax = 0.0048512 //.01 * 0.432 *0.66 //// (.01~.05 siemens/cm2)
}

axon{
        //cm = 1
        insert leak
        insert kdrax
        insert naax
        
        e_leak = -50 // (mV)
        g_leak = .03e-3* 0.648 // (siemens/cm2)
        
        ek = -80
        gbar_kdrax = .1* 0.648  // (.1~.5 siemens/cm2)
        ena = 50		// (mV)
        gbar_naax = 0.2* 0.648	// (siemens/cm2)

}


all = new SectionList()
soma all.append()
neu1 all.append()
siz all.append()
axon all.append()
		}

obfunc connect2target() { localobj nc
	soma nc = new NetCon(&v(1), $o1)
	nc.threshold = 0
	if (numarg() == 2) { $o2 = nc }
	return nc
}

endtemplate LargeCell2
*/

begintemplate LargeCell3
	public soma, axon, siz, neu1,connect2target
	create soma, axon, siz, neu1

	public ncl
	objref ncl

	proc init() {
		ncl = new List()

		/////// topology ////////
    		create soma, neu1, siz, axon
		connect soma(1), neu1(0)
	//connect neu1(1), siz(0)
    //connect siz(1),axon(0)
    connect neu1(1),axon(0)

        
        
        soma {
		// INSERTING MECHANISMS FROM MOD FILES
			insert leak
			insert a2
			insert bkkca
			insert skkca
			insert kd2
			insert cal
			insert cat
			insert Nn 
			insert nap2
			insert pool		
					
		
		// DEFINE MORPHOLOGY	
			L= 120		       // micron
			diam= 90		       // micron
			cm = 1.5		       // uF/cm2
			Ra = 50		       // ohm-cm
			nseg=1
            
            //surface area: 46652.65
            //volume: 7.63×10^5


		// SETTING REVERSAL POTENTIALS FOR INSERTED MECHANISMS
			ek = -80			
			e_leak = -51 		
			ena = 55			
			eca = 45
			enn = -30
			
		// SETTING CONDUCTANCES (AND MORE) FOR INSERTED MECHANISMS (S/cm^2)
	
			g_leak = 1.5e-4 
			g_a2 =  0.0025			
			g_bkkca = 0.011
			gbar_skkca = 8.79e-4	    
			g1_kd2 = 9e-4   		
			g2_kd2 = 9e-5    		
			g_cal = 0.00013	 	
			g_cat = 0.00031			
			gbar_Nn = 1.05e-4				 
			g_nap2 = 1.9e-4
			q_nap2 = 0        				
			f_pool = 20e6  			
			
			
}
		
neu1{ 
		// INSERT MECHANISIMS FROM MOD FILES
			insert leak
			//insert cat
			//insert kd2
			//insert pool	
			insert casLC
			insert nap2
            
            //insert casax
			insert catax
			insert kcaax
			insert capoolax
			
		// DEFINE MORPHOLOGY
			L = 1380 			//um
			diam = 20 				//um
			cm = 1.5 			//uF/cm^2    
			Ra = 50		//ohm-cm
			nseg = 1
		
		// SET REVERSAL POTENTIALS
			ek = -80
			e_leak = -51 	
			eca = 45            
		
		// SET CONDUCTANCES (S/cm^2)
			g_leak = 1.5e-4
			//g_cat = 0.00031
			//g1_kd2 = 9e-4   		
			//g2_kd2 = 9e-5   				
			//f_pool = 20e6 
			g_nap2 = 0//1.9e-4
			q_nap2 = 0 	

            gkcabar_kcaax = 0.0001//0.500e-3	// (siemens/cm2)

            gcatbar_catax = 0.003//0.0552e-3	// (siemens/cm2)
			           
            
			taucas_capoolax = 303	// (ms)
			cainf_capoolax = 0//500e-6 	// (mM)
			casi = cainf_capoolax            

}
        
        
/////// geometry ////////
		siz {
			nseg=1
			L=31.83098861837907e3		// (micrometer)
			diam=1e3		// (micrometer)
	    	}

		axon {
			nseg=1
			L=31.83098861837907e3		// (micrometer)
			diam=1e3		// (micrometer)
            
            //surface area: 1,670,000
            //volume: 2.43×10^7
            
            //L= 120		       // micron
			//diam= 90
		}

		/////// biophysics //////
		siz {
			cm = 9e-3		// (microF/cm2)
			Ra = 500	// (ohm-cm)
			//Rm = 30000	// (ohm-cm2)
			
			insert leakax
			insert catax
			insert casax
			insert kdrax
			insert kAax
			insert kcaax
			insert capoolax
	    	}

		axon {
			cm = 1.5e-3		// (microF/cm2)
			Ra = 500	// (ohm-cm)
			//Rm = 30000	// (ohm-cm2)
			
			insert leakax
			glbar_leakax = 0.0000018e-3 //1/Rm // (siemens/cm2)
			el_leakax = -60	// (mV)

			insert naax
			insert kdrax
            
            insert catax
			insert kcaax
			insert capoolax

		}
		siz {
			el_leakax = -50	// (mV)
			glbar_leakax = 0.000045e-3 //1/Rm // (siemens/cm2)

			ecaax = 120		// (mV)
			gcatbar_catax = 0.0552e-3	// (siemens/cm2)
			gcasbar_casax = 0.055e-3 	// (siemens/cm2)
			
			ekax = -80		// (mV)
			gkdrbar_kdrax = 1.890e-3	// (siemens/cm2)
			gkAbar_kAax = 0.001 //0.200e-3	// (siemens/cm2)
			gkcabar_kcaax = 0.500e-3	// (siemens/cm2)

			taucas_capoolax = 303	// (ms)
			cainf_capoolax = 500e-6 	// (mM)
			casi = cainf_capoolax
		}

		axon {
			enaax= 50		// (mV)
			gnabar_naax = 0.300e-3	// (siemens/cm2)

			ek =-30			// (mV)
			gkdrbar_kdrax = 0.0425e-3 	// (siemens/cm2)
            
            
            
            
            gkcabar_kcaax = 0//0.500e-3	// (siemens/cm2)

            gcatbar_catax = 0//0.0552e-3	// (siemens/cm2)
			           
            
			taucas_capoolax = 303	// (ms)
			cainf_capoolax = 0//500e-6 	// (mM)
			casi = cainf_capoolax   

		}		
}  
    obfunc connect2target() { localobj nc
	soma nc = new NetCon(&v(1), $o1)
	nc.threshold = 0
	if (numarg() == 2) { $o2 = nc }
	return nc
}

endtemplate LargeCell3


begintemplate LargeCell4
	public soma, siz, neu1, synlist, all, connect2target, synapses, type
	public x, y, vcl
	create soma
	create siz
	create neu1									// make sections

	objref synlist, all, syn_, vcl


proc init() {

	strdef type
	type = "LC"
	
	x = 0
	y = 0

	synlist = new List()
	

// CONNECT COMPARTMENTS
	connect soma(1), neu1(0)
	connect neu1(1), siz(0)

	
soma {
		// INSERTING MECHANISMS FROM MOD FILES
			insert leak
			insert a2
			insert bkkca
			insert skkca
			insert kd2
			insert cal
			insert cat
			insert Nn 
			insert nap2
			insert pool			
					
		
		// DEFINE MORPHOLOGY	
			L= 120		       // micron
			diam= 90		       // micron
			cm = 1.5		       // uF/cm2
			Ra = 50		       // ohm-cm
			nseg=1


		// SETTING REVERSAL POTENTIALS FOR INSERTED MECHANISMS
			ek = -80			
			e_leak = -51 		
			ena = 55			
			eca = 45
			enn = -30
			
		// SETTING CONDUCTANCES (AND MORE) FOR INSERTED MECHANISMS (S/cm^2)
	
			g_leak = 1.5e-4 
			g_a2 =  0.0025			
			g_bkkca = 0.011
			gbar_skkca = 8.79e-4	    
			g1_kd2 = 9e-4   		
			g2_kd2 = 9e-5    		
			g_cal = 0.00013	 	
			g_cat = 0.00031			
			gbar_Nn = 1.05e-4				 
			g_nap2 = 1.9e-4
			q_nap2 = 0        				
			f_pool = 20e6  			
			
			
}
		
neu1{ 
		// INSERT MECHANISIMS FROM MOD FILES
			insert leak
			insert cat
            insert casSC
            insert kcaSC
			//insert kd2
			insert pool	
			
		// DEFINE MORPHOLOGY
			L = 1380 			//um
			diam = 20 				//um
			cm = 1.5 			//uF/cm^2    
			Ra = 50		//ohm-cm
			//Rm = 100
			nseg = 1
		
		// SET REVERSAL POTENTIALS
			//ek = -80
			e_leak = -51 		
		
		// SET CONDUCTANCES (S/cm^2)
			g_leak = 1.5e-4
			g_cat = 0//0.00016
            gcasbar_casSC = 0//0.000065
            gkcabar_kcaSC = 0//0.00079
			//g1_kd2 = 9e-4   		
			//g2_kd2 = 9e-5   				
			f_pool = 20e6  	

}
		
		
siz{		// model identically to soma
		// INSERT MECHANISIMS FROM MOD FILES
			insert leak
			insert nasiz
			insert kdsiz
            insert cat
            insert casSC
            insert kcaSC
            insert pool
		
		// DEFINE MORPHOLOGY
			L= 108		// micron
			diam= 20			// micron
			cm = 1.5			// uF/cm2
			Ra = 50 			//ohm-cm
			//Rm = 9999
			nseg=1    
		
		// SET REVERSAL POTENTIALS
			ek = -80			
			e_leak = -51			
			ena = 55	
            f_pool = 20e6  

		
		// SET CONDUCTANCES
			g_leak = 1.5e-4 
			g_nasiz = 0.5 
			g_kdsiz = 0.22
            g_cat = 0//0.00016
            gcasbar_casSC = 0//0.000065
            gkcabar_kcaSC = 0//0.00079
			
}


all = new SectionList()
soma all.append()
neu1 all.append()
siz all.append()
		}
		
obfunc connect2target() { localobj nc
	soma nc = new NetCon(&v(1), $o1)
	nc.threshold = 0
	if (numarg() == 2) { $o2 = nc }
	return nc
}

endtemplate LargeCell4