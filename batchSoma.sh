#!/bin/bash

#SBATCH --job-name=[RSsoma.%j]
#SBATCH -o RSsoma.o%j.txt
#SBATCH -e RSsoma.e%j.txt
#SBATCH -p Lewis
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem-per-cpu 30G
#SBATCH --time 0-23:00

module load intel/intel-2016-update2
module load nrn/nrn-mpi-7.4
module load openmpi/openmpi-2.0.0

module list

nrnivmodl

srun nrniv -mpi RST_MakeCells_Soma.hoc

