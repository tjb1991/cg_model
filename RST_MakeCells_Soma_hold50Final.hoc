load_file("nrngui.hoc")
load_file("LargeCell5_Soma.hoc")


////////////////
// Parameters //
////////////////

v_init = -41
celsius = 12
seedLG = 57845
seedLE = 583
seedA = 1356
seedBkkca = 90
seedKd1 = 236
seedKd2 = 8970
seedCal = 480
seedNn = 2689
seedSkkca = 3546
seedCat = 4805   
seedNap = 964
seedDC = 199

Trials = 3000 


// Building objects 
objref Cell[Trials], ranLeakG, ranLeakE, ranA, ranBkkca, ranKd1, ranKd2, ranCal, ranNn, ranSkkca, ranCat, ranNap, cells, nc, cellid, pc, nil 
objref vecLeakG, vecLeakE, vecA, vecBkkca, vecKd1, vecKd2, vecCal, vecNn, vecSkkca, vecCat, vecNap
objref vol[Trials], Voltage1[Trials], Voltage2[Trials], Stim[Trials]
objref save1, save2Pre, save2Post
objref Rin[Trials], Tau[Trials]
objref CI[Trials] 
objref f1, injT, injAMP
objref Vrest[Trials]
objref SaveMatPara, SaveMatPre, SaveMatPost
objref HoldingPre, save2Hold, vclamp[Trials], CurrentPre[Trials]

pc = new ParallelContext()
cells = new List()


ranLeakG = new Random(seedLG)
ranLeakE = new Random(seedLE)
ranA = new Random(seedA)
ranBkkca = new Random(seedBkkca)
ranKd1 = new Random(seedKd1)
ranKd2 = new Random(seedKd2)
ranCal = new Random(seedCal)
ranNn = new Random(seedNn)
ranSkkca = new Random(seedSkkca)
ranCat = new Random(seedCat)
ranNap = new Random(seedNap)


ranLeakG.uniform(6.2,97)      // Calculated based on Jing's Rin data and surface area of (soma+neurite)
ranLeakE.uniform(-67.1,-50.6)	  // Set based on Jing's Vrest data (used to be 53 - 39, but expanded to test the holding voltage)
ranA.uniform(17.2,190)
ranBkkca.uniform(7.9,61)
ranKd1.uniform(16.5,127)
ranKd2.uniform(91,500)         // Don't know range yet, so make it tight
ranCal.uniform(6.5,13)
ranNn.uniform(7,15)
ranSkkca.uniform(88,200)       // Don't know range yet, so make it tight
ranCat.uniform(16,31)
ranNap.uniform(7.6,35)		  // Upper limit set based on Jing's poster result

vecLeakG = new Vector(Trials)
vecLeakE = new Vector(Trials)
vecA = new Vector(Trials)
vecBkkca = new Vector(Trials)
vecKd1 = new Vector(Trials)
vecKd2 = new Vector(Trials)
vecCal = new Vector(Trials)
vecNn = new Vector(Trials)
vecSkkca = new Vector(Trials)
vecCat = new Vector(Trials)
vecNap = new Vector(Trials)


SaveMatPre = new Matrix(5101,Trials)     //nrow = tstop/dt + 1
SaveMatPost = new Matrix(5101,Trials)


for i=0, Trials-1{

	vecLeakG.x[i] = (ranLeakG.repick())*1e-5
	vecLeakE.x[i] = ranLeakE.repick()
	vecA.x[i] = (ranA.repick())*1e-5 
	vecBkkca.x[i] = (ranBkkca.repick())*1e-4 
	vecKd1.x[i] = (ranKd1.repick())*1e-5
	vecKd2.x[i] = (ranKd2.repick())*1e-6
	vecCal.x[i] = (ranCal.repick())*1e-5
	vecNn.x[i] = (ranNn.repick())*1e-5
	vecSkkca.x[i] = (ranSkkca.repick())*1e-5
	vecCat.x[i] = (ranCat.repick())*1e-5
	vecNap.x[i] = (ranNap.repick())*1e-5 


}


for(i=pc.id;i<Trials;i+=pc.nhost) { 

Cell = new LargeCell()

cells.append(Cell)                          // Add this cell to the list (otherwise its lost!)
	pc.set_gid2node(i, pc.id)                   // Associate “i?with this node id
												// nc = (create netcon object on cell)
	nc = Cell.connect2target(nil) 				// attach spike detector $
	nc.delay = 2
	nc.weight = 1
	pc.cell(i, nc)								// associate gid i with spike detector
												// Associate i with the netcon (so that the cluster 
	 		                                    // knows where the spikes are coming from)													
}


for m = 0, Trials-1{
    if(!pc.gid_exists(m)) { continue }				// Can't connect to target if it doesn't exist 
													// on the node ("continue") skips rest of code
	
	cellid = pc.gid2cell(m)                     	// get GID object from ID

	cellid.soma.g_a2 = vecA.x[m]
	cellid.soma.g_bkkca = vecBkkca.x[m] 
	cellid.soma.g1_kd2 = vecKd1.x[m]
	cellid.soma.g2_kd2 = vecKd2.x[m]
	cellid.soma.g_cal = vecCal.x[m]
	cellid.soma.gbar_Nn = vecNn.x[m]
	cellid.soma.gbar_skkca = vecSkkca.x[m]
	cellid.soma.g_cat = vecCat.x[m]
	cellid.soma.g_nap2 = vecNap.x[m]
	cellid.soma.e_leak = vecLeakE.x[m]
	cellid.soma.g_leak = vecLeakG.x[m]
	cellid.neu1.e_leak = vecLeakE.x[m]
	cellid.neu1.g_leak = vecLeakG.x[m]	
	cellid.soma Stim[m] = new IClamp(0.5)

	Stim[m].del = 300
	Stim[m].dur = 500
	Stim[m].amp = -1
	vol[m] = new Vector()
	vol[m].record(&cellid.soma.v(.5),0.5)


}

// The following codes are equivalent to the "run()" command.
// We must finish the current clamp "Stim" and the recording "vol" before calling them.
 
tstop = 1000
steps_per_ms = 5
dt = 0.2                     // Set an appropriate step size since we will save the data points.
{pc.set_maxstep(10)}
{pc.timeout(0)}
stdinit()
{pc.psolve(tstop)}

// Calculate Rin values and save the parameters.

SaveMatPara = new Matrix(14,Trials)

for m = 0, Trials-1{
    if(!pc.gid_exists(m)) { continue }				// Can't connect to target if it doesn't exist 
													// on the node ("continue") skips rest of code
	
	cellid = pc.gid2cell(m) 
	a = (vol[m].size()-1)/4
	b = (vol[m].size()-1)*(3/4)
	
	Vrest[m] = new Vector(1,0)
	Vrest[m].x[0] = vol[m].x[a]
	Rin[m] = new Vector(1,0)
	Rin[m].x[0] = (vol[m].x[a]-vol[m].x[b])
	Tau[m] = new Vector(1,0)
	Tau[m].x[0] = (Rin[m].x[0]*1.5*1.21)
	
	SaveMatPara.x[0][m] = vecLeakG.x[m]
	SaveMatPara.x[1][m] = vecLeakE.x[m]
	SaveMatPara.x[2][m] = vecA.x[m]
	SaveMatPara.x[3][m] = vecBkkca.x[m]
	SaveMatPara.x[4][m] = vecKd1.x[m]
	SaveMatPara.x[5][m] = vecKd2.x[m]
	SaveMatPara.x[6][m] = vecCal.x[m]
	SaveMatPara.x[7][m] = vecNn.x[m]
	SaveMatPara.x[8][m] = vecSkkca.x[m]
	SaveMatPara.x[9][m] = vecCat.x[m]
	SaveMatPara.x[10][m] = vecNap.x[m]
	SaveMatPara.x[11][m] = Vrest[m].x[0]
	SaveMatPara.x[12][m] = Rin[m].x[0]
	SaveMatPara.x[13][m] = Tau[m].x[0]

	save1 = new File()
	save1.wopen("./lv1-out/somaParameter.txt")
	SaveMatPara.fprint(0,save1,"%f ")
	save1.close() 

}

tstop = 1000
{pc.set_maxstep(10)}
{pc.timeout(0)}
stdinit()
{pc.psolve(tstop)}

// Perform 500 ms voltage clamp to find the average holding current
for m = 0, Trials-1{ 
    if(!pc.gid_exists(m)) { continue }	
	cellid = pc.gid2cell(m)
	
	// vsrc.play(&var or stmt, tvec, 1 = 'continuous'), 
	// When continuous is 1 then linear interpolation is used to define the values between time points.
	 
	cellid.soma vclamp[m] = new SEClamp(0.5)
	vclamp[m].rs = 0.0002 // Mohm:series resistance should be much smaller than input resistance of the cell
	vclamp[m].dur1 = 50
	vclamp[m].amp1 = -50
	vclamp[m].dur2 = 450
	vclamp[m].amp2 = -50
	vclamp[m].dur3 = 50
	vclamp[m].amp3 = -50

	CurrentPre[m] = new Vector()
	CurrentPre[m].record(&vclamp[m].i,0.2)

}

tstop = 550
steps_per_ms = 5
dt = 0.2 
{pc.timeout(0)}
{pc.set_maxstep(10)}
stdinit()
{pc.psolve(tstop)}

// Calculate and save the holding current

HoldingPre = new Matrix(1,Trials)

for m = 0, Trials-1{ 
    if(!pc.gid_exists(m)) { continue }	
cellid = pc.gid2cell(m) 
	
	HoldingPre.x[0][m] = CurrentPre[m].mean(250,2500) // Skip the first and last 50 ms

}

save2Hold = new File()
save2Hold.wopen("./lv1-out/Holding_Pre.txt")
HoldingPre.fprint(0,save2Hold,"%f ")
save2Hold.close()

tstop = 1000
{pc.timeout(0)}
{pc.set_maxstep(10)}
stdinit()
{pc.psolve(tstop)}

// Run, record and save the Stimulus protocol response before applying TEA.


f1 = new File()
f1.ropen("./Stimulus_Protocol_Sources/stim1.txt")
injT = new Vector(100000)
injAMP = new Vector(100000)

for i=0,100000-1 {
	injT.x(i) = 1000*f1.scanvar()
	injAMP.x(i) = f1.scanvar()
	
	if(i<1556) {injAMP.x(i)=0}   //clear out 311.2 ms for holding
	
}

for m = 0, Trials-1{ 
    if(!pc.gid_exists(m)) { continue }	
	cellid = pc.gid2cell(m) 

	// vsrc.play(&var or stmt, tvec, 1 = 'continuous'), 
	// When continuous is 1 then linear interpolation is used to define the values between time points. 
	
	injAMP.add(HoldingPre.x[0][m]) // Add the holding current
	
	cellid.soma CI[m] = new IClamp(0.5)
	injAMP.play(&CI[m].amp,injT,1)
	CI[m].dur = 10e9
	CI[m].del = 0

	Voltage1[m] = new Vector()
	Voltage1[m].record(&cellid.soma.v(.5),0.5)
	
	injAMP.sub(HoldingPre.x[0][m]) // substract the holding current for next cell

}

tstop = 2550
steps_per_ms = 5
dt = 0.2 
{pc.timeout(0)}
{pc.set_maxstep(10)}
stdinit()
{pc.psolve(tstop)}


for m = 0, Trials-1{ 
    if(!pc.gid_exists(m)) { continue }	
cellid = pc.gid2cell(m) 

	SaveMatPre.setcol(m,Voltage1[m])

}

print "Mat Row"
print SaveMatPre.nrow
print "Mat Col"
print SaveMatPre.ncol

save2Pre = new File()
save2Pre.wopen("./lv1-out/Synaptic_Pre.txt")
SaveMatPre.fprint(0,save2Pre,"%f ")
save2Pre.close()

tstop = 1000
{pc.timeout(0)}
{pc.set_maxstep(10)}
stdinit()
{pc.psolve(tstop)}

// Run, record and save the Stimulus protocol response after applying TEA.

for m = 0, Trials-1{ 
    if(!pc.gid_exists(m)) { continue }	
	cellid = pc.gid2cell(m) 

	cellid.soma.g_bkkca = cellid.soma.g_bkkca*0.03
	cellid.soma.g1_kd2 = cellid.soma.g1_kd2*0.03
	cellid.soma.g2_kd2 = cellid.soma.g2_kd2*0.03
	cellid.soma.g_a2 = cellid.soma.g_a2*0.03

	// vsrc.play(&var or stmt, tvec, 1 = 'continuous'), 
	// When continuous is 1 then linear interpolation is used to define the values between time points. 

	cellid.soma CI[m] = new IClamp(0.5)
	injAMP.play(&CI[m].amp,injT,1)
	CI[m].dur = 10e9
	CI[m].del = 0

	Voltage2[m] = new Vector()
	Voltage2[m].record(&cellid.soma.v(.5),0.5)

}

tstop = 2550
steps_per_ms = 5
dt = 0.2  
{pc.timeout(0)}
{pc.set_maxstep(10)}
stdinit()
{pc.psolve(tstop)}



for m = 0, Trials-1{ 
    if(!pc.gid_exists(m)) { continue }	
cellid = pc.gid2cell(m) 

	SaveMatPost.setcol(m,Voltage2[m])

}


save2Post = new File()
save2Post.wopen("./lv1-out/Synaptic_Post.txt")
SaveMatPost.fprint(0,save2Post,"%f ")
save2Post.close()



tstop = 1000
{pc.timeout(0)}
{pc.set_maxstep(10)}
stdinit()
{pc.psolve(tstop)}


pc.barrier()


{pc.runworker()}
{pc.done()}
