%This program performs the LV1 rejection (Vrest, Rin, Tau, pre- and post-TEA).
clear;
close all;

Trials = 3000; % Equals to the total number of cells generated
Para_LV1 = importdata('./lv1-out/SomaParameter.txt');
Holding = importdata('./lv1-out/Holding_Pre.txt');
SynPre_LV1 = importdata('./lv1-out/Synaptic_Pre.txt');
SynPost_LV1 = importdata('./lv1-out/Synaptic_Post.txt');

%% Discard the first 50 ms data points to avoid V_init effect
SynPre_LV1(1:(50/0.5),:) = [];
SynPost_LV1(1:(50/0.5),:) = [];
% Get rid of the last point (which jumps to 0)
SynPre_LV1(end,:) = [];
SynPost_LV1(end,:) = [];

%% The order of parameters:
% Leak_G 
% Leak_E 
% A 
% BKKCa 
% Kd1 
% Kd2
% CaL 
% CAN 
% SKKCa 
% CaT 
% NaP
% Vrest
% Rin 
% Tau
%% Tag the cells
Para_LV1(end+1,:)=1:Trials;
% Reject by Rin
RinPass_LV1 = double.empty(1,0);
for i = 1:Trials
    if Para_LV1(13,i)>=0.852&&Para_LV1(13,i)<=13.3
        RinPass_LV1(end+1) = i;
    end
end

%% Reject by Vrest
VrestPass_LV1 = double.empty(1,0);
for i = 1:Trials
    if Para_LV1(12,i)>=-67.1&&Para_LV1(12,i)<=-50.6 %%if Para_LV1(12,i)>=-53&&Para_LV1(12,i)<=-39 %%Changed 1/13/2019 as per Dr. Nair.
        VrestPass_LV1(end+1) = i;
    end
end

%% Reject by Tau
TauPass_LV1 = double.empty(1,0);
for i = 1:Trials
    if Para_LV1(14,i)>=7.3&&Para_LV1(14,i)<=24.5
        TauPass_LV1(end+1) = i;
    end
end

%% Calculate the Pre-TEA Vpeak and Area
SPtime = [0:0.5:2500-0.5]';
% Baseline for burst No.1 is chosen from V(t=0~0.1s)

for i = 1:length(SynPre_LV1(1,:))
    Baseline_Pre_LV1(1,i)= mean(SynPre_LV1(SPtime<=100,i));
    y = SynPre_LV1(:,i)-Baseline_Pre_LV1(1,i);
    AreaPre_LV1(1,i) = trapz(SPtime,y);
    PeakPre_LV1(1,i) = max(SynPre_LV1(:,i))-Baseline_Pre_LV1(1,i);
    % Tag the cells
    Baseline_Pre_LV1(2,i)= Para_LV1(15,i);
    AreaPre_LV1(2,i)= Para_LV1(15,i);
    PeakPre_LV1(2,i)= Para_LV1(15,i);
end

% Reject based on Pre-TEA response (Area under the curve)
APrePass_LV1 = double.empty(1,0);
for i = 1:length(AreaPre_LV1(1,:))
    if AreaPre_LV1(1,i)>=4640&&AreaPre_LV1(1,i)<=18373
       APrePass_LV1(end+1)=AreaPre_LV1(2,i);
    end
end

% Reject based on Pre-TEA response (maximum dV above baseline)
PPrePass_LV1 = double.empty(1,0);
for i = 1:length(PeakPre_LV1(1,:))
    if PeakPre_LV1(1,i)>=10.03&&PeakPre_LV1(1,i)<=33.76
        PPrePass_LV1(end+1)=PeakPre_LV1(2,i);
    end
end

%% Reject by Baseline_pre (avoid overshadowing)
BPrePass_LV1 = double.empty(1,0);
for i = 1:length(Baseline_Pre_LV1(1,:))
    if Baseline_Pre_LV1(1,i)>=Para_LV1(12,i)-3&&Baseline_Pre_LV1(1,i)<=Para_LV1(12,i)+3
        BPrePass_LV1(end+1)=Baseline_Pre_LV1(2,i);
    end
end

%% Calculate Post-TEA Vpeak and Area

% Baseline for burst No.1 is chosen from V(t=0~0.1s)

for i = 1:length(SynPost_LV1(1,:))
    Baseline_Post_LV1(1,i)= mean(SynPost_LV1(SPtime<=100,i));
    y = SynPost_LV1(:,i)-Baseline_Post_LV1(1,i);
    AreaPost_LV1(1,i) = abs(trapz(SPtime,y)); 
    PeakPost_LV1(1,i) = max(SynPost_LV1(:,i))-Baseline_Post_LV1(1,i);
    % Tag the cells
    Baseline_Post_LV1(2,i)= Para_LV1(15,i);  
    AreaPost_LV1(2,i)= Para_LV1(15,i);
    PeakPost_LV1(2,i)= Para_LV1(15,i);
end

%% Reject by Baseline_post (avoid overshadowing)
BPostPass_LV1 = double.empty(1,0);
for i = 1:length(Baseline_Post_LV1(1,:))
    if Baseline_Post_LV1(1,i)>=Para_LV1(12,i)-3&&Baseline_Post_LV1(1,i)<=Para_LV1(12,i)+3
        BPostPass_LV1(end+1)=Baseline_Post_LV1(2,i);
    end
end


%% Reject based on Post-TEA response (Area under the curve)
APostPass_LV1= double.empty(1,0);
for i = 1:length(AreaPost_LV1(1,:))
    if AreaPost_LV1(1,i)>=5178&&AreaPost_LV1(1,i)<=36853
        APostPass_LV1(end+1)=AreaPost_LV1(2,i);
    end
end

%% Reject based on Post-TEA response (maximum dV above baseline)(Not using)
% ID_PPostFailed = double.empty(1,0);
% for i = 1:length(PeakPost(1,:))
%     if PeakPost(1,i)<11.67||PeakPost(1,i)>39.36
%         ID_PPostFailed(end+1)=i;
%     end
% end

%% Pick out the cells that pass all waveform rejections
% The intersect function can only compare rows
PassivePassIndx_LV1 = intersect(RinPass_LV1,VrestPass_LV1,'stable');

%% Pick out the cells that pass all waveform rejections
C12 = intersect(BPostPass_LV1,BPrePass_LV1,'stable');
C13 = intersect(APrePass_LV1,PPrePass_LV1,'stable');
C14 = intersect(C12,C13,'stable');
WavePassIndx_LV1 = intersect(C14,APostPass_LV1,'stable');

% Pick out the cells that pass LV1
PassIndxLV_1 = intersect(WavePassIndx_LV1,PassivePassIndx_LV1,'stable');
ParaPassLV_1 = Para_LV1(1:11,PassIndxLV_1);

ParaPassLV_1_Complete = Para_LV1(1:14,PassIndxLV_1);
ParaPassLV_1_Complete(15,:) = Holding(1,PassIndxLV_1);

%% Save the cell index at each stage
fileID = fopen('./lv2-in/PassCellIndx_LV1.txt','w');
fprintf(fileID,'RinPass_LV1 ');
fprintf(fileID,'%d ',RinPass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'VrestPass_LV1 ');
fprintf(fileID,'%d ',VrestPass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'APrePass_LV1 ');
fprintf(fileID,'%d ',APrePass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'PPrePass_LV1 ');
fprintf(fileID,'%d ',PPrePass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'BPrePass_LV1 ');
fprintf(fileID,'%d ',BPrePass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'APostPass_LV1 ');
fprintf(fileID,'%d ',APostPass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'BPostPass_LV1 ');
fprintf(fileID,'%d ',BPostPass_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'PassivePassIndx_LV1 ');
fprintf(fileID,'%d ',PassivePassIndx_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'WavePassIndx_LV1 ');
fprintf(fileID,'%d ',WavePassIndx_LV1);
fprintf(fileID,'\n');
fprintf(fileID,'PassIndxLV_1 ');
fprintf(fileID,'%d ',PassIndxLV_1);
fprintf(fileID,'\n');
fclose(fileID);

%% Save the Parameters into a new file for LV2-Intact cell generation
SavingLoc = './lv2-in/LV2ParaInput.txt';
save(SavingLoc,'ParaPassLV_1','-ascii','-double');

SavingLoc1 = './lv2-in/LV2ParaInput_Complete.txt';
save(SavingLoc1,'ParaPassLV_1_Complete','-ascii','-double');


    
size(PassIndxLV_1)